#!/bin/bash

display_usage() {
	echo "This script require the followin environmant variables : ROLE_ID, SECRET_ID, VAULT_ADDR"
	echo -e "\nUsage:\n./vaultkvexp.sh KV_path \n"
}

if [  $# -ne 1 ]; then
	display_usage
	exit 1
fi

echo "Get token"
export VAULT_TOKEN=$(vault write -tls-skip-verify auth/approle/login \
      role_id=$ROLE_ID \
      secret_id=$SECRET_ID | grep token | grep -v token_ | awk '{ print $2}')

echo "Get KV"
#vault kv get -tls-skip-verify -format json $1 | jq -r '.data|to_entries|map("export \(.key)='"'"'\(.value|tostring|gsub("'"'"'"; "'"'"'\\'"''"'"; "g"))'"'"'")|.[]'>env.tmp

# Fix difference beween kv-v1 & kv-v2
VAULT_DATA=$(vault kv get -format json  "$1" | jq -r '.data.data')
if [ "$VAULT_DATA" = "null" ]; then
    vault kv get -tls-skip-verify -format json "$1" | jq -r '.data|to_entries|map("export \(.key)=\"\(.value|tostring)\"")|.[]'>env.tmp
else
    vault kv get -tls-skip-verify -format json "$1" | jq -r '.data.data|to_entries|map("export \(.key)=\"\(.value|tostring)\"")|.[]'>env.tmp
fi

sed -i -e 's/\$/\\$/g' env.tmp
#source env.tmp
#rm -f env.tmp
