###############################################################
###                Get Google GKE Vault Secrets
###############################################################

data "vault_generic_secret" "google" {
  path = "${var.vault_kv_path}/google"
}

locals {
  # GCP credentials
  cluster_gcp_credentials     = data.vault_generic_secret.google.data_json

  # type                        = data.vault_generic_secret.google.data["type"]
  # private_key_id              = data.vault_generic_secret.google.data["private_key_id"]
  # private_key                 = data.vault_generic_secret.google.data["private_key"]
  # client_email                = data.vault_generic_secret.google.data["client_email"]
  # auth_uri                    = data.vault_generic_secret.google.data["auth_uri"]
  # token_uri                   = data.vault_generic_secret.google.data["token_uri"]
  # auth_provider_x509_cert_url = data.vault_generic_secret.google.data["auth_provider_x509_cert_url"]
  # client_x509_cert_url        = data.vault_generic_secret.google.data["client_x509_cert_url"]
  
  project_id                  = data.vault_generic_secret.google.data["project_id"]
  client_id                   = data.vault_generic_secret.google.data["client_id"]
  client_email                = data.vault_generic_secret.google.data["client_email"]

  region                      = data.vault_generic_secret.google.data["region"]
  az_list                     = data.vault_generic_secret.google.data["az_list"]
  image_type                  = data.vault_generic_secret.google.data["image_type"]
  disk_type                   = data.vault_generic_secret.google.data["disk_type"]
  network_name                = data.vault_generic_secret.google.data["network_name"]
  sub_network                 = data.vault_generic_secret.google.data["sub_network"]
}
