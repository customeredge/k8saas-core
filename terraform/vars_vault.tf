###############################################################
###                   Common Providers
###############################################################

provider "vault" {
  version = "~> 2.15"
}

###############################################################
###                   Get Vault Secrets
###############################################################

data "vault_generic_secret" "docker" {
  path = "${var.vault_kv_path}/docker"
}

data "vault_generic_secret" "consul" {
  path = "${var.vault_kv_path}/consul"
}

data "vault_generic_secret" "s3" {
  path = "${var.vault_kv_path}/s3"
}

data "vault_generic_secret" "rancher" {
  path = "${var.vault_kv_path}/rancher"
}

data "vault_generic_secret" "suricate" {
  path = "${var.vault_kv_path}/suricate"
}

data "vault_generic_secret" "vmc" {
  path = "${var.vault_kv_path}/vmc"
}


locals {
  script_version            = "0.1.2"

  json_data = jsondecode(file(var.parameters))
  
  env_prefix                = local.json_data.parameters.cluster.client.env_prefix
  env_prefix_dns            = local.json_data.parameters.cluster.admin.use_env_prefix_dns == "true" || local.json_data.parameters.cluster.admin.use_env_prefix_dns == true ? (local.env_prefix == "pr" ? "" : "${local.env_prefix}-") : ""
 
  additional_storage        = lookup(local.json_data.parameters.cluster.client,"additional_storage","null") != "null" ? local.json_data.parameters.cluster.client.additional_storage : ""
  additional_storage_size   = element(split(",", local.additional_storage),2)
  
  snapshot_to_restore       = lookup(local.json_data.parameters.cluster.client,"snapshot_to_restore","null") != "null" ? local.json_data.parameters.cluster.client.snapshot_to_restore : ""

  enable_etcd_ssd           = lookup(local.json_data.parameters.cluster.admin,"enable_etcd_ssd","null") != "null" ? local.json_data.parameters.cluster.admin.enable_etcd_ssd : "false"

  cluster_cidr              = lookup(local.json_data.parameters.cluster.admin,"cluster_cidr","null") != "null" ? local.json_data.parameters.cluster.admin.cluster_cidr : "false"
  service_cidr              = lookup(local.json_data.parameters.cluster.admin,"service_cidr","null") != "null" ? local.json_data.parameters.cluster.admin.service_cidr : "false"
  
  docker_username           = data.vault_generic_secret.docker.data["DOCKER_USERNAME"]
  docker_password           = data.vault_generic_secret.docker.data["DOCKER_PASSWORD"]
  
  consul_address            = data.vault_generic_secret.consul.data["CONSUL_HTTP_ADDR"]
  consul_token              = data.vault_generic_secret.consul.data["CONSUL_HTTP_TOKEN"]

  s3_service_endpoint       = data.vault_generic_secret.s3.data["S3_SERVICE_ENDPOINT"]
  s3_bucket_name            = data.vault_generic_secret.s3.data["S3_BUCKET_NAME"]
  s3_region                 = data.vault_generic_secret.s3.data["S3_REGION"]
  s3_access_key             = data.vault_generic_secret.s3.data["S3_ACCESS_KEY"]
  s3_secret_key             = data.vault_generic_secret.s3.data["S3_SECRET_KEY"]

  rancher_url               = data.vault_generic_secret.rancher.data["RANCHER_URL"]
  rancher_access_key        = data.vault_generic_secret.rancher.data["RANCHER_ACCESS_KEY"]
  rancher_secret_key        = data.vault_generic_secret.rancher.data["RANCHER_SECRET_KEY"]

  suricate_endpoint         = data.vault_generic_secret.suricate.data["SURICATE_ENDPOINT"]
  suricate_appid            = data.vault_generic_secret.suricate.data["SURICATE_APPID"]
  suricate_ca               = data.vault_generic_secret.suricate.data["SURICATE_CA"]
  suricate_cert             = data.vault_generic_secret.suricate.data["SURICATE_CERT"]
  suricate_key              = data.vault_generic_secret.suricate.data["SURICATE_KEY"]

  vmc_endpoint_target1      = data.vault_generic_secret.vmc.data["VMC_ENDPOINT_TARGET1"]
  #vmc_endpoint_target2      = data.vault_generic_secret.vmc.data["VMC_ENDPOINT_TARGET2"]
  vmc_username              = data.vault_generic_secret.vmc.data["VMC_USERNAME"]
  vmc_password              = data.vault_generic_secret.vmc.data["VMC_PASSWORD"]
}
