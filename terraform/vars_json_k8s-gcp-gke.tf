###############################################################
###                   Get json parameters
###############################################################

locals {
  cluster_gcp_credentials     = jsonencode(local.json_data.parameters.infrastructure.cloud)
  
  project_id                  = local.json_data.parameters.infrastructure.cloud.project_id
  client_id                   = local.json_data.parameters.infrastructure.cloud.client_id
  client_email                = local.json_data.parameters.infrastructure.cloud.client_email

  region                      = local.json_data.parameters.infrastructure.cloud.region
  az_list                     = local.json_data.parameters.infrastructure.cloud.az_list
  image_type                  = local.json_data.parameters.infrastructure.cloud.image_type
  disk_type                   = local.json_data.parameters.infrastructure.cloud.disk_type
  network_name                = local.json_data.parameters.infrastructure.cloud.network_name
  sub_network                 = local.json_data.parameters.infrastructure.cloud.sub_network
}
