# Show Rancher private IPs
# output "rancher_private_ips" {
#     value = [module.rancher.rancher_private_ips]
# }

# Show Master private IPs
output "master_private_ips" {
    value = [module.k8s_openstack_rke.master_private_ips]
}

# Show Etcd private IPs
output "etcd_private_ips" {
    value = [module.k8s_openstack_rke.etcd_private_ips]
}

# Show Worker private IPs
output "worker_private_ips" {
    value = [module.k8s_openstack_rke.worker_private_ips]
}

# Show Worker GPU private IPs
output "worker_gpu_private_ips" {
    value = [module.k8s_openstack_rke.worker_gpu_private_ips]
}

# output "tfstate_output_content" {
#     value = module.k8s_openstack_rke.tfstate_output
# }

# # Show cluster import commands
# output "node-command-rke" {
#   value = [module.k8s_openstack_rke.node-command-rke]
# }

#######################################################################

# Show cluster id
output "cluster_id" {
    value = module.k8s_openstack_rke.cluster_id
}

# Show cluster Kubeconfig
output "kube_config" {
    value = module.k8s_openstack_rke.kube_config
}

# # Rancher URL
# output "rancher_url" {
#     #value = module.k8s_openstack_rke.rancher_url
#     value = local.rancher_url
# }

# # Rancher ACCESS KEY
# output "rancher_access_key" {
#     #value = module.k8s_openstack_rke.rancher_access_key
#     value = local.rancher_access_key
# }

# # Rancher ACCESS KEY
# output "rancher_secret_key" {
#     #value = module.k8s_openstack_rke.rancher_secret_key
#     value = local.rancher_secret_key
# }
