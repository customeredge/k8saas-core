###############################################################
###                   Get json parameters
###############################################################

locals {
  os_auth_url               = local.json_data.parameters.infrastructure.cloud.OS_AUTH_URL
  os_domain_name            = local.json_data.parameters.infrastructure.cloud.OS_DOMAIN_NAME
  os_tenant_id              = local.json_data.parameters.infrastructure.cloud.OS_TENANT_ID
  os_username               = local.json_data.parameters.infrastructure.cloud.OS_USERNAME
  os_password               = local.json_data.parameters.infrastructure.cloud.OS_PASSWORD
  os_region_name            = local.json_data.parameters.infrastructure.cloud.OS_REGION_NAME
  os_private_key            = local.json_data.parameters.infrastructure.cloud.OS_PRIVATE_KEY
  cluster_ca                = local.json_data.parameters.infrastructure.cloud.CLUSTER_CA
  az_list                   = local.json_data.parameters.infrastructure.cloud.az_list
  sg_default                = local.json_data.parameters.infrastructure.cloud.sg_default
  sg_api                    = local.json_data.parameters.infrastructure.cloud.sg_api
  sg_etcd                   = local.json_data.parameters.infrastructure.cloud.sg_etcd
  sg_worker                 = local.json_data.parameters.infrastructure.cloud.sg_worker
  cloud                     = local.json_data.parameters.infrastructure.cloud.cloud
  key_pair                  = local.json_data.parameters.infrastructure.cloud.key_pair
  internal_network_name     = local.json_data.parameters.infrastructure.cloud.internal_network_name
  external_network_name     = local.json_data.parameters.infrastructure.cloud.external_network_name
  internal_subnet_id        = local.json_data.parameters.infrastructure.cloud.internal_subnet_id
  external_subnet_id        = local.json_data.parameters.infrastructure.cloud.external_subnet_id
  loadbalancer_api_eip      = local.json_data.parameters.infrastructure.cloud.loadbalancer_api_eip
  loadbalancer_worker_eip   = local.json_data.parameters.infrastructure.cloud.loadbalancer_worker_eip
  use_openstack_cinder_csi  = local.json_data.parameters.infrastructure.cloud.use_openstack_cinder_csi
}
