# KUBERNETES
variable "cluster_name" {}
variable "cluster_fqdn" {}
variable "k8s_version" {}
variable "cluster_ca" {}
variable "enable_etcd_snapshots" {}
variable "k8s_backup_interval" {type = number}
variable "k8s_backup_retention" {type = number}

# METADATA
variable "env_prefix" {}
variable "env_prefix_dns" {}
variable "project_stream" {}
variable "cloud" {}
variable "domain" {}

# SCALE
variable "count_master_etcd" {}
variable "count_master" {}
variable "count_etcd" {}
variable "count_worker" {}
variable "count_worker_gpu" {}

# IMAGE
variable "image_name" {}

# FLAVORS
variable "flavor_name_master_etcd" {}
variable "flavor_name_master" {}
variable "flavor_name_etcd" {}
variable "flavor_name_worker" {}
variable "flavor_name_worker_gpu" {}

# TFSTATE COMMON
variable "use_remote_state" {}
variable "backend_path" {}
variable "backend_tf" {}

# CONSUL
variable "use_consul" {}
variable "CONSUL_HTTP_ADDR" {}
variable "CONSUL_HTTP_TOKEN" {}

# S3
variable "s3_service_endpoint" {}
variable "s3_bucket_name" {}
variable "s3_region" {}
variable "s3_access_key" {}
variable "s3_secret_key" {}

# STORAGE
variable "use_openstack_cinder_csi" {}
variable "additional_storage" {}
variable "additional_storage_size" {}
variable "enable_etcd_ssd" {}

# LOGGING
variable "enable_logging" {}
variable "suricate_endpoint" {}
variable "suricate_appid" {}
variable "suricate_ca" {}
variable "suricate_cert" {}
variable "suricate_key" {}

# MONITORING & METRICS
variable "enable_cluster_monitoring" {}
variable "vmc_endpoint_target1" {}
#variable "vmc_endpoint_target2" {}
variable "vmc_username" {}
variable "vmc_password" {}

# NETWORK
variable "az_list" {type = list(string)}
variable "sg_default" {}
variable "internal_network_name" {}
variable "external_network_name" {}
variable "internal_subnet_id" {}
variable "external_subnet_id" {}
variable "create_loadbalancer" {}
variable "loadbalancer_api_eip" {}
variable "loadbalancer_worker_eip" {}
variable "sg_api" {}
variable "sg_etcd" {}
variable "sg_worker" {}
variable "cni" {}
variable "cluster_domain" {}
variable "cluster_cidr" {}
variable "service_cidr" {}
variable "cluster_dns_server" {}

# SSH
variable "ssh_user" {}
variable "key_pair" {}

# PROXY
variable "http_proxy" {}
variable "https_proxy" {}
variable "no_proxy" {}

# ETCD
variable "etcd_volume_size_gb" {}

# DOCKER
variable "docker_version" {}
variable "docker_volume_size_gb" {}
variable "registry_mirror" {}
variable "docker_username" {}
variable "docker_password" {}

# OS INFRA
variable "os_auth_url" {}
variable "os_domain_name" {}
variable "os_tenant_id" {}
variable "os_username" {}
variable "os_password" {}
variable "os_region_name" {}
variable "os_private_key" {}

# RANCHER
variable "rancher_url" {}
variable "rancher_access_key" {}
variable "rancher_secret_key" {}

# HARBOR
variable "use_harbor" {}

# K8BOARDS
variable "use_k8boards" {}

# SNAPSHOT RESTORE
variable "snapshot_to_restore" {}

