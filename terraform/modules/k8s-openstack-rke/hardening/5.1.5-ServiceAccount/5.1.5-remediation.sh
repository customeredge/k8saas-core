for ns in $(kubectl get ns --no-headers -o custom-columns=":metadata.name")
do
        kubectl patch serviceaccount default -n $ns -p "automountServiceAccountToken: false"
done