#!/bin/bash -e

export KUBECONFIG=${KUBECONFIG:-"/$HOME/.kube/config"}

RESULT=pass

kubectl version > /dev/null
if [ $? -ne 0 ]; then
  echo "fail: kubectl failed"
  exit 1
fi

for namespace in $(kubectl get namespaces -A -o json | jq -r '.items[].metadata.name'); do
  policy_count=$(kubectl get networkpolicy -n ${namespace} -o json | jq '.items | length')
  if [ ${policy_count} -eq 0 ]; then
    kubectl apply -f network-policy-allow-all-ingress.yaml -n ${namespace} 1>/dev/null && echo "applied: ${namespace}" || (echo "fail: ${namespace}" && RESULT=fail)
    #exit 1
  else
    echo "ok: ${namespace}"
  fi
done

echo ${RESULT}