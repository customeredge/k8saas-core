# Show Master private IPs
output "master_private_ips" {
    value = openstack_compute_instance_v2.instance_openstack_master.*.network.0.fixed_ip_v4
}

# Show Etcd private IPs
output "etcd_private_ips" {
    value = openstack_compute_instance_v2.instance_openstack_etcd.*.network.0.fixed_ip_v4
}

# Show Worker private IPs
output "worker_private_ips" {
    value = openstack_compute_instance_v2.instance_openstack_worker.*.network.0.fixed_ip_v4
}

# Show Worker GPU private IPs
output "worker_gpu_private_ips" {
    value = openstack_compute_instance_v2.instance_openstack_worker_gpu.*.network.0.fixed_ip_v4
}

##################################################################################################

# Show cluster id
output "cluster_id" {
    value = rancher2_cluster.cluster.id
}

# Show cluster Kube_config
output "kube_config" {
    value = rancher2_cluster.cluster.kube_config
}

# # Rancher URL
# output "rancher_url" {
#     value = var.rancher_url
# }

# # Rancher ACCESS KEY
# output "rancher_access_key" {
#     value = var.rancher_access_key
# }

# # Rancher ACCESS KEY
# output "rancher_secret_key" {
#     value = var.rancher_secret_key
# }