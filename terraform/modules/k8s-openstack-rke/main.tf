###############################################################
###                    Local Providers
###############################################################

terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "~> 1.44.0"
    }
    rancher2 = {
      source = "rancher/rancher2"
      version = "~> 1.20.1"
      #version = "~> 1.17.2"
    }
  }
}

provider "openstack" {
  insecure = "true"

  auth_url    = var.os_auth_url
  domain_name = var.os_domain_name
  tenant_id   = var.os_tenant_id
  user_name   = var.os_username
  password    = var.os_password
  region      = var.os_region_name
}

provider "rancher2" {
  insecure   = "true"

  api_url     = var.rancher_url
  access_key  = var.rancher_access_key
  secret_key  = var.rancher_secret_key
}

###############################################################
###                        LOCALES
###############################################################

resource "local_file" "cloud_config" {
    content     = templatefile("${path.module}/cloud-config/cloud-config.tmpl", { os_auth_url = var.os_auth_url, os_domain_name = var.os_domain_name, os_tenant_id = var.os_tenant_id, os_username = var.os_username, os_password = var.os_password, os_region_name = var.os_region_name, internal_subnet_id = var.internal_subnet_id })
    filename = "${path.root}/cloud-config.yaml"
}

###############################################################
###            GET TERRAFORM REMOTE STATE
###############################################################

# Get data from backend (TF state)
# CONSUL
data "terraform_remote_state" "remote_consul" {
  count = var.use_remote_state == true || var.use_remote_state == "true" ? (var.backend_tf == "consul" ? 1 : 0) : 0

  backend = var.backend_tf
  
  config = {
    path    = var.backend_path
  }
}

# S3
data "terraform_remote_state" "remote_s3" {
  count = var.use_remote_state == true || var.use_remote_state == "true" ? (var.backend_tf == "s3" ? 1 : 0) : 0
 
  backend = var.backend_tf
  
  config = {
    bucket = var.s3_bucket_name
    key    = var.backend_path
    region = var.s3_region

    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
  }
}

# ###############################################################
# ###            CREATE onetime ETCD SNAPSHOT
# ###############################################################

# Create a new rancher2 Etcd Backup
resource "rancher2_etcd_backup" "etcd_backup_before" {
  #count = var.use_remote_state ? lookup(data.terraform_remote_state.remote[0].outputs, "cluster_id", "null") != "null" ? 1 : 0 : 0
  count = (
    var.use_remote_state ? (
      var.backend_tf == "consul" ? 
        (lookup(data.terraform_remote_state.remote_consul[0].outputs, "kube_config", "null") != "null" ? 1 : 0) :
      var.backend_tf == "s3" ? 
        (lookup(data.terraform_remote_state.remote_s3[0].outputs, "kube_config", "null") != "null" ? 1 : 0) : 0
    ) : 0
  )

  cluster_id = (
    var.backend_tf == "consul" ? 
    data.terraform_remote_state.remote_consul[0].outputs.cluster_id : 
    var.backend_tf == "s3" ? 
    data.terraform_remote_state.remote_s3[0].outputs.cluster_id : null
  )
  #cluster_id = rancher2_cluster.cluster.id
  #name = "k8s-cluster-${var.cluster_name}-etcd-backup-onetime-before"
  name = "etcd-before"
  #filename = "${timestamp()}_etcd-before"

  backup_config {
    enabled                 = var.enable_etcd_snapshots
    interval_hours          = var.k8s_backup_interval
    retention               = var.k8s_backup_retention
    s3_backup_config {
      access_key            = var.s3_access_key
      secret_key            = var.s3_secret_key
      bucket_name           = var.s3_bucket_name
      folder                = "k8s-cluster-${var.cluster_name}-etcd-backup"
      region                = var.s3_region
      endpoint              = var.s3_service_endpoint
    }
  }

  depends_on = [
    data.terraform_remote_state.remote_consul,
    data.terraform_remote_state.remote_s3
  ]
}

###############################################################
###               CREATE CLUSTER IN RANCHER
###############################################################

# Create cluster import resource in Rancher2
resource "rancher2_cluster" "cluster" {
  name = var.cluster_name
    
  # Hardening
  enable_network_policy = true
  #default_pod_security_policy_template_id = "restricted"
  default_pod_security_policy_template_id = "unrestricted"
 
  cluster_auth_endpoint {
    enabled = true
    fqdn = var.cluster_fqdn
    ca_certs = var.cluster_ca
  }

  rke_config {
    kubernetes_version = var.k8s_version
    ssh_agent_auth = false
    ignore_docker_version = true
    
    network {
      plugin = var.cni
    }
    services {
      etcd {
        # Hardening
        gid = "52034"
        uid = "52034"

        # for etcd snapshots
        backup_config {
        enabled             = var.enable_etcd_snapshots
        interval_hours      = var.k8s_backup_interval
        retention           = var.k8s_backup_retention
          s3_backup_config {
            access_key        = var.s3_access_key
            secret_key        = var.s3_secret_key
            bucket_name       = var.s3_bucket_name
            folder            = "k8s-cluster-${var.cluster_name}-etcd-backup"
            region            = var.s3_region
            endpoint          = var.s3_service_endpoint
          }
        }
        extra_env = [
          "HTTP_PROXY=${var.http_proxy}",
          "HTTPS_PROXY=${var.https_proxy}",
          "NO_PROXY=${var.no_proxy}"
        ]
      }
      kube_api {
        service_cluster_ip_range = var.service_cidr
        # Hardening
        always_pull_images = false
        pod_security_policy = true
        service_node_port_range = "30000-32767"
        secrets_encryption_config {
          enabled = true
        }
        audit_log {
          enabled = true
        }
        event_rate_limit {
          enabled = true
        }
        extra_args = {
          # # Enable PodPreset (deprecated)
          #runtime-config = "settings.k8s.io/v1alpha1=true"
          #enable-admission-plugins = "PodPreset,PodSecurityPolicy,NodeRestriction,ServiceAccount"
          enable-admission-plugins = "PodSecurityPolicy,NodeRestriction,ServiceAccount"
        }
      }
      kube_controller {
        cluster_cidr = var.cluster_cidr
        service_cluster_ip_range = var.service_cidr
        # Hardening
        extra_args = {
          address = "127.0.0.1"
          feature-gates = "RotateKubeletServerCertificate=true"
          profiling = "false"
          terminated-pod-gc-threshold = "1000"
          cluster-signing-cert-file = "/etc/kubernetes/ssl/kube-ca.pem"
          cluster-signing-key-file = "/etc/kubernetes/ssl/kube-ca-key.pem"
        }
      }
      kubelet {
        cluster_domain = var.cluster_domain
        cluster_dns_server = var.cluster_dns_server
        # Hardening
        fail_swap_on = "false"
        generate_serving_certificate = "true"
        extra_args = {
          anonymous-auth = "false"
          event-qps = "0"
          feature-gates = "RotateKubeletServerCertificate=true"
          make-iptables-util-chains = "true"
          protect-kernel-defaults = "true"
          streaming-connection-idle-timeout = "1800s"
          tls-cipher-suites = "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_128_GCM_SHA256"
          #cloud-provider = "external"
        }
      }
      scheduler {
        # Hardening
        extra_args = {
          address = "127.0.0.1"
          profiling = "false"
        }
      }
    }

    /* cloud_provider {
      name = "external"
    } */

    cloud_provider {
      name = "openstack"
      openstack_cloud_provider {
        global {
          username = var.os_username
          password = var.os_password
          auth_url = var.os_auth_url
          tenant_id = var.os_tenant_id
          domain_name = var.os_domain_name
          region = var.os_region_name
        }
        load_balancer {
          subnet_id = var.internal_subnet_id
        }
      }
    }

    addons = <<EOL
---
apiVersion: v1
kind: Namespace
metadata:
  name: ingress-nginx
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: default-psp-role
  namespace: ingress-nginx
rules:
- apiGroups:
  - extensions
  resourceNames:
  - default-psp
  resources:
  - podsecuritypolicies
  verbs:
  - use
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: default-psp-rolebinding
  namespace: ingress-nginx
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: default-psp-role
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:serviceaccounts
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:authenticated
---
apiVersion: v1
kind: Namespace
metadata:
  name: cattle-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: default-psp-role
  namespace: cattle-system
rules:
- apiGroups:
  - extensions
  resourceNames:
  - default-psp
  resources:
  - podsecuritypolicies
  verbs:
  - use
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: default-psp-rolebinding
  namespace: cattle-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: default-psp-role
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:serviceaccounts
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:authenticated
---
apiVersion: policy/v1beta1
kind: PodSecurityPolicy
metadata:
  name: restricted
spec:
  requiredDropCapabilities:
  - NET_RAW
  privileged: false
  allowPrivilegeEscalation: false
  defaultAllowPrivilegeEscalation: false
  fsGroup:
    rule: RunAsAny
  runAsUser:
    rule: MustRunAsNonRoot
  seLinux:
    rule: RunAsAny
  supplementalGroups:
    rule: RunAsAny
  volumes:
  - emptyDir
  - secret
  - persistentVolumeClaim
  - downwardAPI
  - configMap
  - projected
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: psp:restricted
rules:
- apiGroups:
  - extensions
  resourceNames:
  - restricted
  resources:
  - podsecuritypolicies
  verbs:
  - use
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: psp:restricted
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: psp:restricted
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:serviceaccounts
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:authenticated
---
EOL

  }

  timeouts {
    create = "30m"
  }
  depends_on = [rancher2_etcd_backup.etcd_backup_before]
}

###############################################################
###                       INSTANCES
###############################################################

# Get Image data
data "openstack_images_image_v2" "myimage" {
  name = var.image_name
  most_recent = true

  depends_on = [rancher2_cluster.cluster]
}

# Create K8S Master(s) + etcd(s)
resource "openstack_compute_instance_v2" "instance_openstack_master_etcd" {
  count = var.count_master_etcd
  name = "${var.env_prefix}-${var.project_stream}-${var.cloud}-${var.cluster_name}-master-${format("%02d", count.index + 1)}"
  availability_zone = var.az_list[count.index % length(var.az_list)]
  image_name = var.image_name
  flavor_name = var.flavor_name_master_etcd
  key_pair = var.key_pair
  security_groups = concat([var.sg_default], [var.sg_api])
  stop_before_destroy = "true"

  network {
    name = var.internal_network_name
  }

  block_device {
    source_type           = "image"
    uuid                  = data.openstack_images_image_v2.myimage.id
    destination_type      = "volume"
    boot_index            = 0
    volume_size           = 40
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = var.docker_volume_size_gb
    boot_index            = 1
    delete_on_termination = true
  }

  /* block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = var.etcd_volume_size_gb
    boot_index            = 2
    delete_on_termination = true
    volume_type           = "SSD"
  } */
    
  lifecycle {
    ignore_changes = [image_name, image_id, block_device]
  }

  # Wait 150s while chef is configuring the instance
  provisioner "remote-exec"  {
    connection {
      type = "ssh"
      user = var.ssh_user
      host = self.network.0.fixed_ip_v4
      private_key = replace(var.os_private_key,"\\n","\n")
    }
    inline = [
       "[ -f /etc/chef/client.rb ] && ls -lh /etc/chef/ && sudo sed -i \"s|pr-chef.service.sd.diod.tech|pr-chef.service.fe.sd.diod.tech|\" /etc/chef/client.rb && cat /etc/chef/client.rb && echo 'sleeping 150s...' && sleep 150 || true"
    ]
  }
  
  metadata = {
    chef_runlist = "\"recipe[DIOD-MINIMAL-SETUP::SSSD]\""
    diod_env = var.cloud == "vdr" ? "vdr-${lower(var.env_prefix)}" : "DIOD-${upper(var.env_prefix)}"
    diod_enabler = var.cloud
    diod_perimeter = var.project_stream
  }

  depends_on = [data.openstack_images_image_v2.myimage]
}

# Create K8S Master(s)
resource "openstack_compute_instance_v2" "instance_openstack_master" {
  count = var.count_master_etcd < 1 ? var.count_master : 0
  name = "${var.env_prefix}-${var.project_stream}-${var.cloud}-${var.cluster_name}-master-${format("%02d", count.index + 1)}"
  availability_zone = var.az_list[count.index % length(var.az_list)]
  image_name = var.image_name
  flavor_name = var.flavor_name_master
  key_pair = var.key_pair
  security_groups = concat([var.sg_default], [var.sg_api])
  stop_before_destroy = "true"

  network {
    name = var.internal_network_name
  }

  block_device {
    source_type           = "image"
    uuid                  = data.openstack_images_image_v2.myimage.id
    destination_type      = "volume"
    boot_index            = 0
    volume_size           = 40
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = var.docker_volume_size_gb
    boot_index            = 1
    delete_on_termination = true
  }

  lifecycle {
    ignore_changes = [image_name, image_id, block_device]
  }

  # Wait 150s while chef is configuring the instance
  provisioner "remote-exec"  {
    connection {
      type = "ssh"
      user = var.ssh_user
      host = self.network.0.fixed_ip_v4
      private_key = replace(var.os_private_key,"\\n","\n")
    }
    inline = [
       "[ -f /etc/chef/client.rb ] && sudo sed -i \"s|pr-chef.service.sd.diod.tech|pr-chef.service.fe.sd.diod.tech|\" /etc/chef/client.rb && echo 'sleeping 150s...' && sleep 150 || true"
    ]
  }

  metadata = {
    chef_runlist = "\"recipe[DIOD-MINIMAL-SETUP::SSSD]\""
    diod_env = var.cloud == "vdr" ? "vdr-${lower(var.env_prefix)}" : "DIOD-${upper(var.env_prefix)}"
    diod_enabler = var.cloud
    diod_perimeter = var.project_stream
  }

  depends_on = [data.openstack_images_image_v2.myimage]
}

# Create K8S Etcd(s)
resource "openstack_compute_instance_v2" "instance_openstack_etcd" {
  count = var.count_master_etcd < 1 ? var.count_etcd : 0
  name = "${var.env_prefix}-${var.project_stream}-${var.cloud}-${var.cluster_name}-etcd-${format("%02d", count.index + 1)}"
  availability_zone = var.az_list[count.index % length(var.az_list)]
  image_name = var.image_name
  flavor_name = var.flavor_name_etcd
  key_pair = var.key_pair
  security_groups = concat([var.sg_default], [var.sg_etcd])
  stop_before_destroy = "true"

  network {
    name = var.internal_network_name
  }

  block_device {
    source_type           = "image"
    uuid                  = data.openstack_images_image_v2.myimage.id
    destination_type      = "volume"
    boot_index            = 0
    volume_size           = 40
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = var.docker_volume_size_gb
    boot_index            = 1
    delete_on_termination = true
  }

  lifecycle {
    ignore_changes = [image_name, image_id, block_device]
  }

  # Wait 150s while chef is configuring the instance
  provisioner "remote-exec"  {
    connection {
      type = "ssh"
      user = var.ssh_user
      host = self.network.0.fixed_ip_v4
      private_key = replace(var.os_private_key,"\\n","\n")
    }
    inline = [
       "[ -f /etc/chef/client.rb ] && sudo sed -i \"s|pr-chef.service.sd.diod.tech|pr-chef.service.fe.sd.diod.tech|\" /etc/chef/client.rb && echo 'sleeping 150s...' && sleep 150 || true"
    ]
  }
  
  metadata = {
    chef_runlist = "\"recipe[DIOD-MINIMAL-SETUP::SSSD]\""
    diod_env = var.cloud == "vdr" ? "vdr-${lower(var.env_prefix)}" : "DIOD-${upper(var.env_prefix)}"
    diod_enabler = var.cloud
    diod_perimeter = var.project_stream
  }
  
  depends_on = [data.openstack_images_image_v2.myimage]
}

# Create K8S Worker(s)
resource "openstack_compute_instance_v2" "instance_openstack_worker" {
  count = var.count_worker
  name = "${var.env_prefix}-${var.project_stream}-${var.cloud}-${var.cluster_name}-worker-${format("%02d", count.index + 1)}"
  availability_zone = var.az_list[count.index % length(var.az_list)]
  image_name = var.image_name
  flavor_name = var.flavor_name_worker
  key_pair = var.key_pair
  security_groups = concat([var.sg_default], [var.sg_worker])
  stop_before_destroy = "true"

  network {
    name = var.internal_network_name
  }

  block_device {
    source_type           = "image"
    uuid                  = data.openstack_images_image_v2.myimage.id
    destination_type      = "volume"
    boot_index            = 0
    volume_size           = 40
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = var.docker_volume_size_gb
    boot_index            = 1
    delete_on_termination = true
  }

  lifecycle {
    ignore_changes = [image_name, image_id, block_device]
  }

  # Wait 150s while chef is configuring the instance
  provisioner "remote-exec"  {
    connection {
      type = "ssh"
      user = var.ssh_user
      host = self.network.0.fixed_ip_v4
      private_key = replace(var.os_private_key,"\\n","\n")
    }
    inline = [
       "[ -f /etc/chef/client.rb ] && sudo sed -i \"s|pr-chef.service.sd.diod.tech|pr-chef.service.fe.sd.diod.tech|\" /etc/chef/client.rb && echo 'sleeping 150s...' && sleep 150 || true"
    ]
  }

  metadata = {
    chef_runlist = "\"recipe[DIOD-MINIMAL-SETUP::SSSD]\""
    diod_env = var.cloud == "vdr" ? "vdr-${lower(var.env_prefix)}" : "DIOD-${upper(var.env_prefix)}"
    diod_enabler = var.cloud
    diod_perimeter = var.project_stream
  }

  depends_on = [data.openstack_images_image_v2.myimage]
}

# Create K8S Worker(s) with GPU
resource "openstack_compute_instance_v2" "instance_openstack_worker_gpu" {
  count = var.count_worker_gpu
  name = "${var.env_prefix}-${var.project_stream}-${var.cloud}-${var.cluster_name}-workergpu-${format("%02d", count.index + 1)}"
  availability_zone = var.az_list[count.index % length(var.az_list)]
  image_name = var.image_name
  flavor_name = var.flavor_name_worker_gpu
  key_pair = var.key_pair
  security_groups = concat([var.sg_default], [var.sg_worker])
  stop_before_destroy = "true"

  network {
    name = var.internal_network_name
  }
  
  block_device {
    source_type           = "image"
    uuid                  = data.openstack_images_image_v2.myimage.id
    destination_type      = "volume"
    boot_index            = 0
    volume_size           = 40
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = var.docker_volume_size_gb
    boot_index            = 1
    delete_on_termination = true
  }

  lifecycle {
    ignore_changes = [image_name, image_id, block_device]
  }

  # Wait 150s while chef is configuring the instance
  provisioner "remote-exec"  {
    connection {
      type = "ssh"
      user = var.ssh_user
      host = self.network.0.fixed_ip_v4
      private_key = replace(var.os_private_key,"\\n","\n")
    }
    inline = [
       "[ -f /etc/chef/client.rb ] && sudo sed -i \"s|pr-chef.service.sd.diod.tech|pr-chef.service.fe.sd.diod.tech|\" /etc/chef/client.rb && echo 'sleeping 150s...' && sleep 150 || true"
    ]
  }

  metadata = {
    chef_runlist = "\"recipe[DIOD-MINIMAL-SETUP::SSSD]\""
    diod_env = var.cloud == "vdr" ? "vdr-${lower(var.env_prefix)}" : "DIOD-${upper(var.env_prefix)}"
    diod_enabler = var.cloud
    diod_perimeter = var.project_stream
  }

  depends_on = [data.openstack_images_image_v2.myimage]
}

###############################################################
###          ADDITIONAL SSD DISKS/VOLUMES ON MASTERS+ETCD
###############################################################

resource "openstack_blockstorage_volume_v3" "additional_disks_ssd_master_etcd" {
  count = var.enable_etcd_ssd == true || var.enable_etcd_ssd == "true" ? var.count_master_etcd : 0

  region                = var.os_region_name
  availability_zone     = var.az_list[count.index % length(var.az_list)]
  name                  = "${openstack_compute_instance_v2.instance_openstack_master_etcd[count.index].name}-etcd-ssd-disk"
  description           = "Master etcd ssd additional disk"
  size                  = var.etcd_volume_size_gb
  enable_online_resize  = true
  volume_type           = "SSD"

  depends_on = [openstack_compute_instance_v2.instance_openstack_master_etcd]
}

resource "openstack_compute_volume_attach_v2" "additional_disks_ssd_master_etcd_attach" {
  count = var.enable_etcd_ssd == true || var.enable_etcd_ssd == "true" ? var.count_master_etcd : 0

  instance_id = openstack_compute_instance_v2.instance_openstack_master_etcd[count.index].id
  volume_id   = openstack_blockstorage_volume_v3.additional_disks_ssd_master_etcd[count.index].id

  depends_on = [openstack_blockstorage_volume_v3.additional_disks_ssd_master_etcd]
}

###############################################################
###          ADDITIONAL SSD DISKS/VOLUMES ON ETCD
###############################################################

resource "openstack_blockstorage_volume_v3" "additional_disks_ssd_etcd" {
  count = var.enable_etcd_ssd == true || var.enable_etcd_ssd == "true" ? var.count_etcd : 0

  region                = var.os_region_name
  availability_zone     = var.az_list[count.index % length(var.az_list)]
  name                  = "${openstack_compute_instance_v2.instance_openstack_etcd[count.index].name}-etcd-ssd-disk"
  description           = "Etcd ssd additional disk"
  size                  = var.etcd_volume_size_gb
  enable_online_resize  = true
  volume_type           = "SSD"

  depends_on = [openstack_compute_instance_v2.instance_openstack_etcd]
}

resource "openstack_compute_volume_attach_v2" "additional_disks_ssd_etcd_attach" {
  count = var.enable_etcd_ssd == true || var.enable_etcd_ssd == "true" ? var.count_etcd : 0

  instance_id = openstack_compute_instance_v2.instance_openstack_etcd[count.index].id
  volume_id   = openstack_blockstorage_volume_v3.additional_disks_ssd_etcd[count.index].id

  depends_on = [openstack_blockstorage_volume_v3.additional_disks_ssd_etcd]
}

###############################################################
###            ADDITIONAL DISKS/VOLUMES ON WORKERS
###############################################################

resource "openstack_blockstorage_volume_v3" "additional_disks_workers" {
  count = var.additional_storage != "" ? var.count_worker : 0

  region                = var.os_region_name
  availability_zone     = var.az_list[count.index % length(var.az_list)]
  name                  = "${openstack_compute_instance_v2.instance_openstack_worker[count.index].name}-additional-disk"
  description           = "Worker additional disk"
  size                  = var.additional_storage_size
  enable_online_resize  = true

  depends_on = [openstack_compute_instance_v2.instance_openstack_worker]
}

resource "openstack_compute_volume_attach_v2" "additional_disks_workers_attach" {
  count = var.additional_storage != "" ? var.count_worker : 0

  instance_id = openstack_compute_instance_v2.instance_openstack_worker[count.index].id
  volume_id   = openstack_blockstorage_volume_v3.additional_disks_workers[count.index].id

  depends_on = [openstack_blockstorage_volume_v3.additional_disks_workers]
}

###############################################################
###                    LOAD BALANCER API
###############################################################

resource "openstack_lb_loadbalancer_v2" "elb_openstack_api" {
  count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name          = "elb-${var.cluster_name}-api"
  vip_subnet_id = var.internal_subnet_id

  depends_on = [
    openstack_compute_instance_v2.instance_openstack_master_etcd,
    openstack_compute_instance_v2.instance_openstack_master
  ]
}

resource "openstack_networking_floatingip_v2" "elb_eip_openstack_api" {
  count      = (var.create_loadbalancer == true || var.create_loadbalancer == "true") && var.loadbalancer_api_eip == null ? 1 : 0
  pool       = var.external_network_name
  port_id    = openstack_lb_loadbalancer_v2.elb_openstack_api[count.index].vip_port_id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_api]
}

resource "openstack_networking_floatingip_associate_v2" "elb_eip_attach_openstack_api" {
  count       = (var.create_loadbalancer == true || var.create_loadbalancer == "true") && var.loadbalancer_api_eip != null ? 1 : 0
  floating_ip = var.loadbalancer_api_eip
  port_id     = openstack_lb_loadbalancer_v2.elb_openstack_api[count.index].vip_port_id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_api]
}

resource "openstack_lb_listener_v2" "elb_listener_openstack_api" {
  count            = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name             = "listener-${var.cluster_name}-6443"
  protocol         = "TCP"
  protocol_port    = "6443"
  loadbalancer_id  = openstack_lb_loadbalancer_v2.elb_openstack_api[count.index].id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_api]
}

resource "openstack_lb_pool_v2" "elb_pool_openstack_api" {
  count           = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name            = "server_group-${var.cluster_name}-api"
  protocol        = "TCP"
  lb_method       = "ROUND_ROBIN"
  listener_id     = openstack_lb_listener_v2.elb_listener_openstack_api[count.index].id
  persistence {
    type          = "SOURCE_IP"
  }

  depends_on = [
    openstack_lb_loadbalancer_v2.elb_openstack_api,
    openstack_lb_listener_v2.elb_listener_openstack_api
  ]
}

resource "openstack_lb_member_v2" "elb_members_openstack_api" {
  count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? (var.count_master_etcd > 0 ? var.count_master_etcd : var.count_master) : 0
  name          = var.count_master_etcd > 0 ? element(openstack_compute_instance_v2.instance_openstack_master_etcd.*.name, count.index) : element(openstack_compute_instance_v2.instance_openstack_master.*.name, count.index)
  address       = var.count_master_etcd > 0 ? element(openstack_compute_instance_v2.instance_openstack_master_etcd.*.network.0.fixed_ip_v4, count.index) : element(openstack_compute_instance_v2.instance_openstack_master.*.network.0.fixed_ip_v4, count.index)
  protocol_port = "6443"
  pool_id       = openstack_lb_pool_v2.elb_pool_openstack_api[0].id
  subnet_id     = var.internal_subnet_id

  depends_on    = [openstack_lb_pool_v2.elb_pool_openstack_api]
}

###############################################################
###                  LOAD BALANCER WORKER
###############################################################

resource "openstack_lb_loadbalancer_v2" "elb_openstack_worker" {
  count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name          = "elb-${var.cluster_name}-worker"
  vip_subnet_id = var.internal_subnet_id

  depends_on = [
    openstack_compute_instance_v2.instance_openstack_worker,
    openstack_compute_instance_v2.instance_openstack_worker_gpu
  ]
}

resource "openstack_networking_floatingip_v2" "elb_eip_openstack_worker" {
  count      = (var.create_loadbalancer == true || var.create_loadbalancer == "true") && var.loadbalancer_worker_eip == null ? 1 : 0
  pool       = var.external_network_name
  port_id    = openstack_lb_loadbalancer_v2.elb_openstack_worker[count.index].vip_port_id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_worker]
}

resource "openstack_networking_floatingip_associate_v2" "elb_eip_attach_openstack_worker" {
  count       = (var.create_loadbalancer == true || var.create_loadbalancer == "true") && var.loadbalancer_worker_eip != null ? 1 : 0
  floating_ip = var.loadbalancer_worker_eip
  port_id     = openstack_lb_loadbalancer_v2.elb_openstack_worker[count.index].vip_port_id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_worker]
}

resource "openstack_lb_listener_v2" "elb_listener_openstack_worker" {
  count            = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name             = "listener-${var.cluster_name}-443"
  protocol         = "TCP"
  protocol_port    = "443"
  loadbalancer_id  = openstack_lb_loadbalancer_v2.elb_openstack_worker[count.index].id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_worker]
}

resource "openstack_lb_listener_v2" "elb_listener_openstack_worker_tcp80" {
  count            = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name             = "listener-${var.cluster_name}-80"
  protocol         = "TCP"
  protocol_port    = "80"
  loadbalancer_id  = openstack_lb_loadbalancer_v2.elb_openstack_worker[count.index].id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_worker]
}

resource "openstack_lb_listener_v2" "elb_listener_openstack_worker_tcp8080" {
  count            = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name             = "listener-${var.cluster_name}-8080"
  protocol         = "TCP"
  protocol_port    = "8080"
  loadbalancer_id  = openstack_lb_loadbalancer_v2.elb_openstack_worker[count.index].id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_worker]
}

resource "openstack_lb_pool_v2" "elb_pool_openstack_worker" {
  count           = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name            = "server_group-${var.cluster_name}-worker"
  protocol        = "TCP"
  lb_method       = "ROUND_ROBIN"
  listener_id     = openstack_lb_listener_v2.elb_listener_openstack_worker[count.index].id
  persistence {
    type          = "SOURCE_IP"
  }

  depends_on = [
    openstack_lb_loadbalancer_v2.elb_openstack_worker,
    openstack_lb_listener_v2.elb_listener_openstack_worker,
    openstack_lb_listener_v2.elb_listener_openstack_worker_tcp80,
    openstack_lb_listener_v2.elb_listener_openstack_worker_tcp8080
  ]
}

resource "openstack_lb_pool_v2" "elb_pool_openstack_worker_tcp80" {
  count           = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name            = "server_group-${var.cluster_name}-worker-tcp80"
  protocol        = "TCP"
  lb_method       = "ROUND_ROBIN"
  listener_id     = openstack_lb_listener_v2.elb_listener_openstack_worker_tcp80[count.index].id
  persistence {
    type          = "SOURCE_IP"
  }

  depends_on = [
    openstack_lb_loadbalancer_v2.elb_openstack_worker,
    openstack_lb_listener_v2.elb_listener_openstack_worker,
    openstack_lb_listener_v2.elb_listener_openstack_worker_tcp80,
    openstack_lb_listener_v2.elb_listener_openstack_worker_tcp8080
  ]
}

resource "openstack_lb_pool_v2" "elb_pool_openstack_worker_tcp8080" {
  count           = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name            = "server_group-${var.cluster_name}-worker-tcp8080"
  protocol        = "TCP"
  lb_method       = "ROUND_ROBIN"
  listener_id     = openstack_lb_listener_v2.elb_listener_openstack_worker_tcp8080[count.index].id
  persistence {
    type          = "SOURCE_IP"
  }

  depends_on = [
    openstack_lb_loadbalancer_v2.elb_openstack_worker,
    openstack_lb_listener_v2.elb_listener_openstack_worker,
    openstack_lb_listener_v2.elb_listener_openstack_worker_tcp80,
    openstack_lb_listener_v2.elb_listener_openstack_worker_tcp8080
  ]
}

resource "openstack_lb_member_v2" "elb_members_openstack_worker" {
  #count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? sum([var.count_worker, var.count_worker_gpu]) : 0
  count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? var.count_worker : 0
  name          = element(openstack_compute_instance_v2.instance_openstack_worker.*.name, count.index)
  address       = element(openstack_compute_instance_v2.instance_openstack_worker.*.network.0.fixed_ip_v4, count.index)
  protocol_port = "443"
  pool_id       = openstack_lb_pool_v2.elb_pool_openstack_worker[0].id
  subnet_id     = var.internal_subnet_id

  depends_on    = [
    openstack_lb_pool_v2.elb_pool_openstack_worker,
    openstack_lb_pool_v2.elb_pool_openstack_worker_tcp80,
    openstack_lb_pool_v2.elb_pool_openstack_worker_tcp8080
  ]
}

resource "openstack_lb_member_v2" "elb_members_openstack_worker_tcp80" {
  #count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? sum([var.count_worker, var.count_worker_gpu]) : 0
  count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? var.count_worker : 0
  name          = element(openstack_compute_instance_v2.instance_openstack_worker.*.name, count.index)
  address       = element(openstack_compute_instance_v2.instance_openstack_worker.*.network.0.fixed_ip_v4, count.index)
  protocol_port = "80"
  pool_id       = openstack_lb_pool_v2.elb_pool_openstack_worker_tcp80[0].id
  subnet_id     = var.internal_subnet_id

  depends_on    = [
    openstack_lb_pool_v2.elb_pool_openstack_worker,
    openstack_lb_pool_v2.elb_pool_openstack_worker_tcp80,
    openstack_lb_pool_v2.elb_pool_openstack_worker_tcp8080
  ]
}

resource "openstack_lb_member_v2" "elb_members_openstack_worker_tcp8080" {
  #count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? sum([var.count_worker, var.count_worker_gpu]) : 0
  count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? var.count_worker : 0
  name          = element(openstack_compute_instance_v2.instance_openstack_worker.*.name, count.index)
  address       = element(openstack_compute_instance_v2.instance_openstack_worker.*.network.0.fixed_ip_v4, count.index)
  protocol_port = "8080"
  pool_id       = openstack_lb_pool_v2.elb_pool_openstack_worker_tcp8080[0].id
  subnet_id     = var.internal_subnet_id

  depends_on    = [
    openstack_lb_pool_v2.elb_pool_openstack_worker,
    openstack_lb_pool_v2.elb_pool_openstack_worker_tcp80,
    openstack_lb_pool_v2.elb_pool_openstack_worker_tcp8080
  ]
}

###############################################################
###                    NODES POST CONF
###############################################################

# Apply Master + etcd nodes post configuration
resource "null_resource" "instance_openstack_master_etcd_postconf" {
  count = var.count_master_etcd
  
  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = element(openstack_compute_instance_v2.instance_openstack_master_etcd.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = replace(var.os_private_key,"\\n","\n")
  }

  provisioner "file" {
    source      = "./script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec"  {
    inline = [
      "while ps aux | grep -i 'apt ' |grep -v grep 1>/dev/null 2>/dev/null;do echo 'Waiting apt being available (sleeping 10s)...' && sleep 10; done",
      "export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy}",
      "chmod +x /tmp/script.sh",
      "[ ! -z \"${var.registry_mirror}\" ] && export REGISTRY_MIRROR=\"--registry-mirror=${var.registry_mirror}\" || export REGISTRY_MIRROR=\"\"",
      "echo \"/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --hardening-etcd --use-additional-storage=${var.docker_volume_size_gb} --use-additional-storage=${var.etcd_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --create-fs=etcd,/var/lib/etcd,${var.etcd_volume_size_gb} --user=${var.ssh_user}\"",
      "/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --hardening-etcd --use-additional-storage=${var.docker_volume_size_gb} --use-additional-storage=${var.etcd_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --create-fs=etcd,/var/lib/etcd,${var.etcd_volume_size_gb} --user=${var.ssh_user}",
      #"echo \"/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --hardening-etcd --use-additional-storage=${var.docker_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --user=${var.ssh_user}\"",
      #"/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --hardening-etcd --use-additional-storage=${var.docker_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --user=${var.ssh_user}",
      "[ ! -z \"${var.docker_username}\" ] && [ ! -z \"${var.docker_password}\" ] && sudo docker login --username ${var.docker_username} --password ${var.docker_password}",
      "$(echo ${rancher2_cluster.cluster.cluster_registration_token.0.node_command}|sed \"s|docker run|docker run -e http_proxy=${var.http_proxy} -e https_proxy=${var.https_proxy} -e no_proxy=${var.no_proxy}|\") --controlplane --etcd"
    ]
  }

  depends_on = [
    openstack_compute_instance_v2.instance_openstack_master_etcd,
    openstack_compute_volume_attach_v2.additional_disks_ssd_master_etcd_attach,
    openstack_compute_volume_attach_v2.additional_disks_ssd_etcd_attach
  ]
}

# Apply Master nodes post configuration
resource "null_resource" "instance_openstack_master_postconf" {
  count = var.count_master_etcd < 1 ? var.count_master : 0
  
  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = element(openstack_compute_instance_v2.instance_openstack_master.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = replace(var.os_private_key,"\\n","\n")
  }

  provisioner "file" {
    source      = "./script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec"  {
    inline = [
      "while ps aux | grep -i 'apt ' |grep -v grep 1>/dev/null 2>/dev/null;do echo 'Waiting apt being available (sleeping 10s)...' && sleep 10; done",
      "export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy}",
      "chmod +x /tmp/script.sh",
      "[ ! -z \"${var.registry_mirror}\" ] && export REGISTRY_MIRROR=\"--registry-mirror=${var.registry_mirror}\" || export REGISTRY_MIRROR=\"\"",
      "echo \"/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --use-additional-storage=${var.docker_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --user=${var.ssh_user}\"",
      "/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --use-additional-storage=${var.docker_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --user=${var.ssh_user}",
      "[ ! -z \"${var.docker_username}\" ] && [ ! -z \"${var.docker_password}\" ] && sudo docker login --username ${var.docker_username} --password ${var.docker_password}",
      "$(echo ${rancher2_cluster.cluster.cluster_registration_token.0.node_command}|sed \"s|docker run|docker run -e http_proxy=${var.http_proxy} -e https_proxy=${var.https_proxy} -e no_proxy=${var.no_proxy}|\") --controlplane"
    ]
  }

  depends_on = [openstack_compute_instance_v2.instance_openstack_master]
}

# Apply Etcd nodes post configuration
resource "null_resource" "instance_openstack_etcd_postconf" {
  count = var.count_master_etcd < 1 ? var.count_etcd : 0
  
  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      
      host = element(openstack_compute_instance_v2.instance_openstack_etcd.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = replace(var.os_private_key,"\\n","\n")
  }

  provisioner "file" {
    source      = "./script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec"  {
    inline = [
      "while ps aux | grep -i 'apt ' |grep -v grep 1>/dev/null 2>/dev/null;do echo 'Waiting apt being available (sleeping 10s)...' && sleep 10; done",
      "export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy}",
      "chmod +x /tmp/script.sh",
      "[ ! -z \"${var.registry_mirror}\" ] && export REGISTRY_MIRROR=\"--registry-mirror=${var.registry_mirror}\" || export REGISTRY_MIRROR=\"\"",
      "echo \"/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --hardening-etcd --use-additional-storage=${var.docker_volume_size_gb} --use-additional-storage=${var.etcd_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --create-fs=etcd,/var/lib/etcd,${var.etcd_volume_size_gb} --user=${var.ssh_user}\"",
      "/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --hardening-etcd --use-additional-storage=${var.docker_volume_size_gb} --use-additional-storage=${var.etcd_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --create-fs=etcd,/var/lib/etcd,${var.etcd_volume_size_gb} --user=${var.ssh_user}",
      "[ ! -z \"${var.docker_username}\" ] && [ ! -z \"${var.docker_password}\" ] && sudo docker login --username ${var.docker_username} --password ${var.docker_password}",
      "$(echo ${rancher2_cluster.cluster.cluster_registration_token.0.node_command}|sed \"s|docker run|docker run -e http_proxy=${var.http_proxy} -e https_proxy=${var.https_proxy} -e no_proxy=${var.no_proxy}|\") --etcd"
    ]
  }

  depends_on = [
    openstack_compute_instance_v2.instance_openstack_etcd,
    openstack_compute_volume_attach_v2.additional_disks_ssd_etcd_attach
  ]
}

# Apply Worker nodes post configuration
resource "null_resource" "instance_openstack_worker_postconf" {
  count = var.count_worker
  
  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      
      host = element(openstack_compute_instance_v2.instance_openstack_worker.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = replace(var.os_private_key,"\\n","\n")
  }

  provisioner "file" {
    source      = "./script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec"  {
    inline = [
      "while ps aux | grep -i 'apt ' |grep -v grep 1>/dev/null 2>/dev/null;do echo 'Waiting apt being available (sleeping 10s)...' && sleep 10; done",
      "export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy}",
      "chmod +x /tmp/script.sh",
      "[ ! -z \"${var.registry_mirror}\" ] && export REGISTRY_MIRROR=\"--registry-mirror=${var.registry_mirror}\" || export REGISTRY_MIRROR=\"\"",
      "[ ! -z \"${var.additional_storage}\" ] && export ADDITIONAL_STORAGE=\"--use-additional-storage=${var.additional_storage_size} --create-fs=${var.additional_storage}\" || export ADDITIONAL_STORAGE=\"\"",
      "echo \"/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --use-additional-storage=${var.docker_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} $ADDITIONAL_STORAGE --user=${var.ssh_user}\"",
      "/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --use-additional-storage=${var.docker_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} $ADDITIONAL_STORAGE --user=${var.ssh_user}",
      "[ ! -z \"${var.docker_username}\" ] && [ ! -z \"${var.docker_password}\" ] && sudo docker login --username ${var.docker_username} --password ${var.docker_password}",
      "echo $(echo ${rancher2_cluster.cluster.cluster_registration_token.0.node_command}|sed \"s|docker run|docker run -e http_proxy=${var.http_proxy} -e https_proxy=${var.https_proxy} -e no_proxy=${var.no_proxy}|\") --worker",
      "$(echo ${rancher2_cluster.cluster.cluster_registration_token.0.node_command}|sed \"s|docker run|docker run -e http_proxy=${var.http_proxy} -e https_proxy=${var.https_proxy} -e no_proxy=${var.no_proxy}|\") --worker"
      #"${rancher2_cluster.cluster.cluster_registration_token.0.node_command} --worker"
    ]
  }

  depends_on = [
    openstack_compute_instance_v2.instance_openstack_worker,
    openstack_compute_volume_attach_v2.additional_disks_workers_attach
  ]
}

# Apply Worker with GPU nodes post configuration
resource "null_resource" "instance_openstack_worker_gpu_postconf" {
  count = var.count_worker_gpu
  
  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      
      host = element(openstack_compute_instance_v2.instance_openstack_worker_gpu.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = replace(var.os_private_key,"\\n","\n")
  }

  provisioner "file" {
    source      = "./script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec"  {
    inline = [
      "while ps aux | grep -i 'apt ' |grep -v grep 1>/dev/null 2>/dev/null;do echo 'Waiting apt being available (sleeping 10s)...' && sleep 10; done",
      "export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy}",
      "chmod +x /tmp/script.sh",
      "[ ! -z \"${var.registry_mirror}\" ] && export REGISTRY_MIRROR=\"--registry-mirror=${var.registry_mirror}\" || export REGISTRY_MIRROR=\"\"",
      "echo \"/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --use-additional-storage=${var.docker_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --gpu --user=${var.ssh_user}\"",
      "/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --use-additional-storage=${var.docker_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --gpu --user=${var.ssh_user}",
      "[ ! -z \"${var.docker_username}\" ] && [ ! -z \"${var.docker_password}\" ] && sudo docker login --username ${var.docker_username} --password ${var.docker_password}",
      "echo $(echo ${rancher2_cluster.cluster.cluster_registration_token.0.node_command}|sed \"s|docker run|docker run -e http_proxy=${var.http_proxy} -e https_proxy=${var.https_proxy} -e no_proxy=${var.no_proxy}|\") --worker",
      "$(echo ${rancher2_cluster.cluster.cluster_registration_token.0.node_command}|sed \"s|docker run|docker run -e http_proxy=${var.http_proxy} -e https_proxy=${var.https_proxy} -e no_proxy=${var.no_proxy}|\") --worker"
      #"${rancher2_cluster.cluster.cluster_registration_token.0.node_command} --worker"
    ]
  }

  depends_on = [openstack_compute_instance_v2.instance_openstack_worker_gpu]
}

###############################################################
###                   WAIT CLUSTER
###############################################################

# Create a new rancher2 Cluster Sync
resource "rancher2_cluster_sync" "wait_cluster" {
  cluster_id =  rancher2_cluster.cluster.id
  wait_monitoring = false
  #wait_catalogs = true
  #state_confirm = 2
  #state_confirm = 120
  #state_confirm = 200
  state_confirm = 300

  timeouts {
    create = "30m"
  }

  depends_on = [
     null_resource.instance_openstack_master_etcd_postconf,
     null_resource.instance_openstack_master_postconf,
     null_resource.instance_openstack_etcd_postconf,
     null_resource.instance_openstack_worker_postconf,
     null_resource.instance_openstack_worker_gpu_postconf
  ]
}

###############################################################
###                  WAIT KUBECONFIG FILE
###############################################################

resource "null_resource" "wait_kubeconfig_file" {
  
  provisioner "local-exec"  {
    command = <<EOC
    echo "${rancher2_cluster.cluster.kube_config}" > ${path.root}/config && \
    while ! cat ${path.root}/config | grep "server" 2>/dev/null 1>/dev/null; do echo "Waiting kubeconfig file being available (sleeping 5s)..." && sleep 5; echo "${rancher2_cluster.cluster.kube_config}" > ${path.root}/config; done && \
    cat ${path.root}/config
    EOC
  }

  depends_on = [rancher2_cluster_sync.wait_cluster]
}

###############################################################
###                 SET KUBE_CONFIG FILE
###############################################################

resource "local_file" "kubeconfig" {
    content     = rancher2_cluster.cluster.kube_config
    filename = "/root/.kube/config"
    
    #depends_on = [rancher2_cluster_sync.wait_cluster]
    depends_on = [null_resource.wait_kubeconfig_file]
}

###############################################################
###              DOWNLOAD KUBECTL
###############################################################

resource "null_resource" "download_kubectl" {
  
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = <<EOC
    kubectl 2>/dev/null 1>/dev/null || \
    (while ! apk --no-cache add curl jq 2>/dev/null;do echo "Waiting apk being available (sleeping 5s)..." && sleep 5; done && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && mv ./kubectl /usr/local/bin && chmod +x /usr/local/bin/kubectl)
    EOC
  }

  depends_on = [local_file.kubeconfig]
}

###############################################################
###              ADD PROXY TO CATTLE SYSTEM
###############################################################

resource "null_resource" "add_proxy" {
  count = var.http_proxy != "" || var.https_proxy != "" ? 1 : 0

  provisioner "local-exec"  {
    command = <<EOC
    while ! kubectl -n cattle-system get DaemonSet/kube-api-auth | grep -v "READY" | awk '{print $2/$4}' | grep 1 1>/dev/null 2>/dev/null; do echo "Waiting cattle-cluster-agent being available (sleeping 5s)..." && sleep 5; done && \
    kubectl -n cattle-system set env deployment/cattle-cluster-agent HTTP_PROXY=${var.http_proxy} && \
    kubectl -n cattle-system set env deployment/cattle-cluster-agent HTTPS_PROXY=${var.https_proxy} && \
    kubectl -n cattle-system set env deployment/cattle-cluster-agent NO_PROXY=${var.no_proxy} && \
    kubectl -n cattle-system set env DaemonSet/cattle-node-agent HTTP_PROXY=${var.http_proxy} && \
    kubectl -n cattle-system set env DaemonSet/cattle-node-agent HTTPS_PROXY=${var.https_proxy} && \
    kubectl -n cattle-system set env DaemonSet/cattle-node-agent NO_PROXY=${var.no_proxy}
    EOC
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl
  ]
}

###############################################################
###              ADD PROXY TO FLEET SYSTEM
###############################################################

resource "null_resource" "add_proxy_fleet" {
  count = var.http_proxy != "" || var.https_proxy != "" ? 1 : 0
  
  provisioner "local-exec"  {
    command = <<EOC
    { { kubectl get namespace cattle-fleet-system 2>/dev/null 1>/dev/null && export NS=cattle-fleet-system; } || \
    { kubectl get namespace fleet-system 2>/dev/null 1>/dev/null && export NS=fleet-system; }; } && \
    ( kubectl -n $NS set env deployment/fleet-agent HTTP_PROXY=${var.http_proxy} && \
      kubectl -n $NS set env deployment/fleet-agent HTTPS_PROXY=${var.https_proxy} && \
      kubectl -n $NS set env deployment/fleet-agent NO_PROXY=${var.no_proxy} ) || true
    EOC
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl
  ]
}

###############################################################
###                    GET SYSTEM DATA
###############################################################

# Get System project data
data "rancher2_project" "project_system" {
  name = "System"
  cluster_id = rancher2_cluster.cluster.id

  depends_on = [rancher2_cluster_sync.wait_cluster]
}

# Get kube-system namespace data
data "rancher2_namespace" "namespace_kubesystem" {
  name = "kube-system"
  project_id = data.rancher2_project.project_system.id

  depends_on = [data.rancher2_project.project_system]
}

###############################################################
###                    CONSUL REGISTER
###############################################################

# Register master into consul
resource "null_resource" "consul_register_service_master_etcd" {
  count = var.use_consul == true || var.use_consul == "true" ? (var.count_master_etcd != 0 ? var.count_master_etcd : 0) : 0

  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = element(openstack_compute_instance_v2.instance_openstack_master_etcd.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }

  provisioner "remote-exec"  {
    inline = [
    <<EOF
cat <<EOT | sudo tee /opt/application/Consul/conf/service-registration.json
{
  "services": [
    {
      "token": "${var.CONSUL_HTTP_TOKEN}",
      "name": "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-api",
      "Address": "${element(openstack_compute_instance_v2.instance_openstack_master_etcd.*.network.0.fixed_ip_v4, count.index)}",
      "Port": 6443,
      "tags": [
        "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-api"
      ],
      "Checks": [
        {
          "Name" : "Kubernetes API health check",
          "status": "passing",
          "http": "http://localhost:10248/healthz",
          "tls_skip_verify": true,
          "method": "GET",
          "Interval": "5s",
          "timeout": "2s"
        }
      ]
    }
  ]
}
EOT
/usr/bin/sudo chown consul:consul /opt/application/Consul/conf/service-registration.json
/usr/bin/sudo systemctl daemon-reload
/usr/bin/sudo systemctl reload consul
    EOF
    ]
  }

  depends_on = [rancher2_cluster_sync.wait_cluster]
}

#########################

# Register master into consul
resource "null_resource" "consul_register_service_master" {
  count = var.use_consul == true || var.use_consul == "true" ? (var.count_master != 0 ? var.count_master : 0) : 0

  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = element(openstack_compute_instance_v2.instance_openstack_master.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }

  provisioner "remote-exec"  {
    inline = [
    <<EOF
cat <<EOT | sudo tee /opt/application/Consul/conf/service-registration.json
{
  "services": [
    {
      "token": "${var.CONSUL_HTTP_TOKEN}",
      "name": "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-api",
      "Address": "${element(openstack_compute_instance_v2.instance_openstack_master.*.network.0.fixed_ip_v4, count.index)}",
      "Port": 6443,
      "tags": [
        "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-api"
      ],
      "Checks": [
        {
          "Name" : "Kubernetes API health check",
          "status": "passing",
          "http": "http://localhost:10248/healthz",
          "tls_skip_verify": true,
          "method": "GET",
          "Interval": "5s",
          "timeout": "2s"
        }
      ]
    }
  ]
}
EOT
/usr/bin/sudo chown consul:consul /opt/application/Consul/conf/service-registration.json
/usr/bin/sudo systemctl daemon-reload
/usr/bin/sudo systemctl reload consul
    EOF
    ]
  }

  depends_on = [rancher2_cluster_sync.wait_cluster]
}

#########################

# Register etcd into consul
resource "null_resource" "consul_register_service_etcd" {
  count = var.use_consul == true || var.use_consul == "true" ? (var.count_etcd != 0 ? var.count_etcd : 0) : 0

  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = element(openstack_compute_instance_v2.instance_openstack_etcd.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }

  provisioner "remote-exec"  {
    inline = [
    <<EOF
cat <<EOT | sudo tee /opt/application/Consul/conf/service-registration.json
{
  "services": [
    {
      "token": "${var.CONSUL_HTTP_TOKEN}",
      "name": "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-etcd",
      "Address": "${element(openstack_compute_instance_v2.instance_openstack_etcd.*.network.0.fixed_ip_v4, count.index)}",
      "Port": 2379,
      "tags": [
        "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-etcd"
      ],
      "Checks": [
        {
          "Name" : "Kubernetes ETCD health check",
          "status": "passing",
          "http": "https://localhost:2379/health",
          "tls_skip_verify": true,
          "method": "GET",
          "Interval": "5s",
          "timeout": "2s"
        }
      ]
    }
  ]
}
EOT
/usr/bin/sudo chown consul:consul /opt/application/Consul/conf/service-registration.json
/usr/bin/sudo systemctl daemon-reload
/usr/bin/sudo systemctl reload consul
    EOF
    ]
  }

  depends_on = [rancher2_cluster_sync.wait_cluster]
}

#########################

# Register worker into consul
resource "null_resource" "consul_register_service_worker" {
  count = var.use_consul == true || var.use_consul == "true" ? (var.count_worker != 0 ? var.count_worker : 0) : 0

  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = element(openstack_compute_instance_v2.instance_openstack_worker.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }

  provisioner "remote-exec"  {
    inline = [
    <<EOF
cat <<EOT | sudo tee /opt/application/Consul/conf/service-registration.json
{
  "services": [
    {
      "token": "${var.CONSUL_HTTP_TOKEN}",
      "name": "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-worker",
      "Address": "${element(openstack_compute_instance_v2.instance_openstack_worker.*.network.0.fixed_ip_v4, count.index)}",
      "Port": 443,
      "tags": [
        "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-worker"
      ],
      "Checks": [
        {
          "Name" : "Kubernetes WORKER health check",
          "status": "passing",
          "http": "http://localhost:10248/healthz",
          "tls_skip_verify": true,
          "method": "GET",
          "Interval": "5s",
          "timeout": "2s"
        }
      ]
    }
  ]
}
EOT
/usr/bin/sudo chown consul:consul /opt/application/Consul/conf/service-registration.json
/usr/bin/sudo systemctl daemon-reload
/usr/bin/sudo systemctl reload consul
    EOF
    ]
  }
          
  depends_on = [rancher2_cluster_sync.wait_cluster]
}

#########################

# Register worker with GPU into consul
resource "null_resource" "consul_register_service_worker_gpu" {
  count = var.use_consul == true || var.use_consul == "true" ? (var.count_worker_gpu != 0 ? var.count_worker_gpu : 0) : 0

  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = element(openstack_compute_instance_v2.instance_openstack_worker_gpu.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }

  provisioner "remote-exec"  {
    inline = [
    <<EOF
cat <<EOT | sudo tee /opt/application/Consul/conf/service-registration.json
{
  "services": [
    {
      "token": "${var.CONSUL_HTTP_TOKEN}",
      "name": "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-worker",
      "Address": "${element(openstack_compute_instance_v2.instance_openstack_worker_gpu.*.network.0.fixed_ip_v4, count.index)}",
      "Port": 443,
      "tags": [
        "${var.env_prefix}-${var.project_stream}-${var.cluster_name}-worker"
      ],
      "Checks": [
        {
          "Name" : "Kubernetes WORKER health check",
          "status": "passing",
          "http": "http://localhost:10248/healthz",
          "tls_skip_verify": true,
          "method": "GET",
          "Interval": "5s",
          "timeout": "2s"
        }
      ]
    }
  ]
}
EOT
/usr/bin/sudo chown consul:consul /opt/application/Consul/conf/service-registration.json
/usr/bin/sudo systemctl daemon-reload
/usr/bin/sudo systemctl reload consul
    EOF
    ]
  }
          
  depends_on = [rancher2_cluster_sync.wait_cluster]
}


###############################################################
###                ADD CLUSTER LOGGING
###############################################################

# Create a new Rancher2 Cluster Logging
resource "rancher2_cluster_logging" "cluster_logging" {
  count = var.enable_logging == true || var.enable_logging == "true" ? 1 : 0
  
  name = "diod-log-suricate"
  cluster_id = rancher2_cluster.cluster.id
  kind = "syslog"
  output_flush_interval = "20"
  output_tags = {"appID" = var.suricate_appid}
  enable_json_parsing = false

  syslog_config {
    endpoint = var.suricate_endpoint
    protocol = "tcp"
    program = "Rancher"
    severity = "notice"
    ssl_verify = false
    enable_tls = true
    certificate = var.suricate_ca
    client_cert = var.suricate_cert
    client_key = var.suricate_key
  }

  depends_on = [rancher2_cluster_sync.wait_cluster]
}

###############################################################
###               CLUSTER MONITORING
###############################################################

# Create cattle-monitoring-system namespace
resource "rancher2_namespace" "namespace_monitoring" {
  count = var.enable_cluster_monitoring == true || var.enable_cluster_monitoring == "true" ? 1 : 0
  #provider = rancher2.admin

  name = "cattle-monitoring-system"
  project_id = data.rancher2_project.project_system.id

  depends_on = [data.rancher2_project.project_system]
}

# Create Victoria Metrics Secret
resource "rancher2_secret" "vmc_creds" {
  count = var.enable_cluster_monitoring == true || var.enable_cluster_monitoring == "true" ? 1 : 0
  #provider = rancher2.admin

  name = "vmc-creds"
  description = "Victoria Metrics Secret"
  project_id = data.rancher2_project.project_system.id
  namespace_id = rancher2_namespace.namespace_monitoring[count.index].id
  data = {
    username = base64encode(var.vmc_username)
    password = base64encode(var.vmc_password)
  }

  lifecycle {
    ignore_changes = [labels]
  }

  depends_on = [rancher2_namespace.namespace_monitoring]
}

# Create Victoria Metrics Certs Secret
resource "rancher2_secret" "vmc_certs" {
  count = var.enable_cluster_monitoring == true || var.enable_cluster_monitoring == "true" ? 1 : 0
  #provider = rancher2.admin

  name = "vmc-certs"
  description = "Victoria Metrics Certs"
  project_id = data.rancher2_project.project_system.id
  namespace_id = rancher2_namespace.namespace_monitoring[count.index].id
  data = {
    "ca.crt" = base64encode(var.cluster_ca)
  }

  lifecycle {
    ignore_changes = [labels]
  }

  depends_on = [rancher2_namespace.namespace_monitoring]
}

# Deploy cluster monitoring app
resource "rancher2_app_v2" "cluster_monitoring" {
  count = var.enable_cluster_monitoring == true || var.enable_cluster_monitoring == "true" ? 1 : 0
  #provider = rancher2.admin

  cluster_id = rancher2_cluster.cluster.id
  name = "rancher-monitoring"
  namespace = "cattle-monitoring-system"
  repo_name = "rancher-charts"
  chart_name = "rancher-monitoring"
  chart_version = "14.5.100"
  #values = file("values.yaml")
  values = <<EOF
prometheus:
  prometheusSpec:
    externalLabels: 
      cluster: ${var.cluster_name}
    remoteWrite:
      - basicAuth:
          password:
            key: password
            name: vmc-creds
          username:
            key: username
            name: vmc-creds
        queueConfig:
          maxShards: 5
        tlsConfig:
          ca:
            secret:
              name: vmc-certs
              key: ca.crt
          insecureSkipVerify: false
        url: '${var.vmc_endpoint_target1}'
EOF  

  depends_on = [
    rancher2_secret.vmc_creds,
    rancher2_secret.vmc_certs
  ]
}

###############################################################
###           DEPLOY NVIDIA K8S DEVICE PLUGIN
###############################################################

# Add a new Rancher2 Cluster Catalog
/* resource "rancher2_catalog" "catalog_nvdp" {
  count = var.count_worker_gpu > 0 ? 1 : 0
  name = "nvdp"
  #cluster_id = rancher2_cluster.cluster.id
  project_id = data.rancher2_project.project_system.id
  url = "https://nvidia.github.io/k8s-device-plugin"
  version = "helm_v3"
  refresh = true
  #scope = "global"
  #scope = "cluster"
  scope = "project"

  lifecycle {
    ignore_changes = [labels]
  }

  depends_on = [data.rancher2_project.project_system]
} */

# Add a new Rancher2 Cluster Catalog
resource "rancher2_catalog_v2" "catalog_nvdp" {
  count = var.count_worker_gpu > 0 ? 1 : 0
  name = "nvdp"
  cluster_id = rancher2_cluster.cluster.id
  #project_id = data.rancher2_project.project_system.id
  url = "https://nvidia.github.io/k8s-device-plugin"
  #version = "helm_v3"
  #refresh = true
  #scope = "global"
  #scope = "cluster"
  #scope = "project"

  lifecycle {
    ignore_changes = [labels]
  }

  depends_on = [data.rancher2_project.project_system]
}


/* # Create a new rancher2 App 
resource "rancher2_app" "app_nvdp" {
  count = var.count_worker_gpu > 0 ? 1 : 0
  catalog_name = "nvdp"
  name = "nvidia-device-plugin"
  description = "Nvidia Device Plugin"
  project_id = data.rancher2_project.project_system.id
  template_name = "nvidia-device-plugin"
  template_version = "0.6.0"
  target_namespace = data.rancher2_namespace.namespace_kubesystem.name
  
  lifecycle {
    ignore_changes = [labels, annotations]
  }

  depends_on = [
    data.rancher2_namespace.namespace_kubesystem,
    rancher2_catalog.catalog_nvdp
    ]
} */

# Create a new rancher2 App
resource "rancher2_app_v2" "app_nvdp" {
  count = var.count_worker_gpu > 0 ? 1 : 0
  name = "nvidia-device-plugin"
  repo_name = "nvdp"
  #description = "Nvidia Device Plugin"
  chart_name = "nvidia-device-plugin"
  chart_version = "0.10.0"
  project_id = data.rancher2_project.project_system.id
  cluster_id = rancher2_cluster.cluster.id
  namespace = data.rancher2_namespace.namespace_kubesystem.name
  
  lifecycle {
    ignore_changes = [labels, annotations]
  }

  depends_on = [
    data.rancher2_namespace.namespace_kubesystem,
    rancher2_catalog_v2.catalog_nvdp
    ]
}

###############################################################
###           DEPLOY CINDER CSI VOLUME PROVISIONER
###############################################################

#
## Usefull for external openstack cloud provider
#

# resource "null_resource" "secret_cloud_config" {
#    count = var.use_openstack_cinder_csi == true || var.use_openstack_cinder_csi == "true" ? 1 : 0

#   provisioner "local-exec"  {
#     command = "kubectl apply -f ${path.root}/cloud-config.yaml"
#   }

#   depends_on = [
#     data.rancher2_project.project_system,
#     local_file.kubeconfig,
#     null_resource.download_kubectl
#   ]
# }

# Add a new Rancher2 Cluster Catalog
resource "rancher2_catalog" "catalog_cpo" {
  count = var.use_openstack_cinder_csi == true || var.use_openstack_cinder_csi == "true" ? 1 : 0
  name = "cpo"
  #cluster_id = rancher2_cluster.cluster.id
  project_id = data.rancher2_project.project_system.id
  url = "https://kubernetes.github.io/cloud-provider-openstack"
  version = "helm_v3"
  refresh = true
  #scope = "global"
  #scope = "cluster"
  scope = "project"

  lifecycle {
    ignore_changes = [project_id, labels]
  }

  #depends_on = [null_resource.secret_cloud_config]
  depends_on = [
    data.rancher2_project.project_system,
    local_file.kubeconfig,
    null_resource.download_kubectl
  ]
}

# Create a new rancher2 App 
resource "rancher2_app" "app_cpo" {
  count = var.use_openstack_cinder_csi == true || var.use_openstack_cinder_csi == "true" ? 1 : 0
  catalog_name = rancher2_catalog.catalog_cpo[count.index].id
  name = "openstack-cinder-csi"
  description = "Cinder CSI volume provisioner"
  project_id = data.rancher2_project.project_system.id
  template_name = "openstack-cinder-csi"
  template_version = "1.4.9"
  target_namespace = data.rancher2_namespace.namespace_kubesystem.name
  
  answers = {
    /* "secret.enabled" = true,
    "secret.name" = "cloud-config", */
    "storageClass.enabled" = false,
    "storageClass.delete.isDefault" = true
  }

  lifecycle {
    ignore_changes = [project_id, labels, annotations]
  }

   timeouts {
     create = "15m"
   }

  depends_on = [data.rancher2_namespace.namespace_kubesystem]
}

# Patch cpo App
resource "null_resource" "patch_cpo" {
  count = var.use_openstack_cinder_csi == true || var.use_openstack_cinder_csi == "true" ? 1 : 0

  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = <<EOC
    while ! kubectl -n kube-system get statefulset openstack-cinder-csi-controllerplugin | grep 1/1 1>/dev/null 2>/dev/null; do echo "Waiting openstack-cinder-csi-controllerplugin being available (sleeping 5s)..." && sleep 5; done && \
    kubectl -n kube-system patch statefulset openstack-cinder-csi-controllerplugin \
        -p '{"spec": {"template": {"spec": {"containers": [{"args": ["--csi-address=$(ADDRESS)","--feature-gates=Topology=true","--timeout=3m"],"name": "csi-provisioner"}]}}}}'
    EOC
  }

  depends_on = [
      local_file.kubeconfig,
      null_resource.download_kubectl,
      rancher2_app.app_cpo
  ]
}

# Add storage class
resource "null_resource" "add_storageclass" {
  count = var.use_openstack_cinder_csi == true || var.use_openstack_cinder_csi == "true" ? 1 : 0

  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = <<EOC
    cd ${path.module}/ && \
    kubectl apply -f ./storageclass
    EOC
  }

  depends_on = [
      local_file.kubeconfig,
      null_resource.download_kubectl,
      rancher2_app.app_cpo
  ]
}

###############################################################
###               DEPLOY HARBOR app
###############################################################

# Create Harbor namespace
resource "rancher2_namespace" "namespace_harbor" {
 count = var.use_harbor == true || var.use_harbor == "true" ? 1 : 0

  name = "harbor"
  #description = "Harbor namespace"
  project_id = data.rancher2_project.project_system.id

  depends_on = [data.rancher2_project.project_system]
}

# Create secret for Harbor TLS
resource "null_resource" "secret_harbor" {
  count = var.use_harbor == true || var.use_harbor == "true" ? 1 : 0
  
  provisioner "local-exec"  {
    #command = "kubectl create secret generic harbor-tls --from-file='tls.crt=./tls.crt' --from-file='tls.key=./tls.key' -n harbor"
    #command = "kubectl create secret generic harbor-tls --from-literal='tls.crt=-----BEGIN CERTIFICATE-----\nMIIEazCCA1OgAwIBAgIUUCHaNXiqunqSEGvKhXR0FAgHFjswDQYJKoZIhvcNAQEL\nBQAwgaExRzBFBgNVBAMMPmhhcmJvci5rOHMtb3BlcmF0aW9ucy5rOHMuZWRnZS5p\nbnRlci1jZG5yZC5vcmFuZ2UtYnVzaW5lc3MuY29tMQ8wDQYDVQQKDAZPcmFuZ2Ux\nEDAOBgNVBAsMB1RHSS9PTFMxFTATBgNVBAcMDFZhbC1EZS1SZXVpbDEPMA0GA1UE\nCAwGRnJhbmNlMQswCQYDVQQGEwJGUjAeFw0yMTA1MjUxNzI2NTBaFw0zMTA1MjMx\nNzI2NTBaMIGhMUcwRQYDVQQDDD5oYXJib3IuazhzLW9wZXJhdGlvbnMuazhzLmVk\nZ2UuaW50ZXItY2RucmQub3JhbmdlLWJ1c2luZXNzLmNvbTEPMA0GA1UECgwGT3Jh\nbmdlMRAwDgYDVQQLDAdUR0kvT0xTMRUwEwYDVQQHDAxWYWwtRGUtUmV1aWwxDzAN\nBgNVBAgMBkZyYW5jZTELMAkGA1UEBhMCRlIwggEiMA0GCSqGSIb3DQEBAQUAA4IB\nDwAwggEKAoIBAQDCDh/9qChbrAnhDO+VRNoDF6VXnmPN1veZfUFrfI7k226bYf9/\nRuacuAXN/LaUSBKjhPur8Q3FvLWNdzMATEsitLp5NjyZpkv9LC54THn1Vu9J42ok\nf2/htYtXaFRumU6GnxARKa35iBpC5RdfcV++xDyY6iMB4B6K8aeorvXUXye+L2po\n5n6l7+UL1TkKm0wDX7nyObCkD3NnK3b+HQefm5Vu/1b9BSjuUDvSF7FAg0BMSB9G\n42BOVxvQOj1il/mOaswXwgJwPVi9+pjpbYJQxKNmooQsh3fjuHIpYPSdwf2Qzavg\ngJOhloG3UWnPbk96D3JggrgSpxiLbp5gcXnrAgMBAAGjgZgwgZUwgZIGA1UdEQSB\nijCBh4I+aGFyYm9yLms4cy1vcGVyYXRpb25zLms4cy5lZGdlLmludGVyLWNkbnJk\nLm9yYW5nZS1idXNpbmVzcy5jb22CRWhhcmJvci1ub3RhcnkuazhzLW9wZXJhdGlv\nbnMuazhzLmVkZ2UuaW50ZXItY2RucmQub3JhbmdlLWJ1c2luZXNzLmNvbTANBgkq\nhkiG9w0BAQsFAAOCAQEAKk1oWMigjiKXXA908bzoX0YDSoA2ZQ42+ppOXiS05/ip\nL+58XX8IKSPtWWiA0afP/c13nGh4+EtMgwq2+PLYalJgACwZinVv954Trqo2Ffem\nSHxjeJSIwUPBL6kDocgUr7dfh+4tQj6v30M/n5vH96LB2uEyksD0Go6qRwejMHkM\nm953rTcUerDObFNpvvRh8ecWXzLr4IAd5ADNoCk8vozwKmfOAUtpyrUnFwISIWPW\n7tUFL8Ld/jGNMi0RlF2weW1WsgKkevX8J4hftv5xeknLfdWBNfA4hKcCOJVlzy4y\nL8Uw3UxqXwxGjxSVadQN2qKjBeDlM/0ZqNX8t2X4Aw==\n-----END CERTIFICATE-----' --from-literal='tls.key=-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDCDh/9qChbrAnh\nDO+VRNoDF6VXnmPN1veZfUFrfI7k226bYf9/RuacuAXN/LaUSBKjhPur8Q3FvLWN\ndzMATEsitLp5NjyZpkv9LC54THn1Vu9J42okf2/htYtXaFRumU6GnxARKa35iBpC\n5RdfcV++xDyY6iMB4B6K8aeorvXUXye+L2po5n6l7+UL1TkKm0wDX7nyObCkD3Nn\nK3b+HQefm5Vu/1b9BSjuUDvSF7FAg0BMSB9G42BOVxvQOj1il/mOaswXwgJwPVi9\n+pjpbYJQxKNmooQsh3fjuHIpYPSdwf2QzavggJOhloG3UWnPbk96D3JggrgSpxiL\nbp5gcXnrAgMBAAECggEBAIdcuP4ElZpUqFN+5YR3EahEj0ZOKwpDoFVF5SNoMHj9\ng+sJceLDf0vkpc/pLybCG5ZKxg9LR7MeDnqOolnb3jFoZgb1SVJYiPsiHYw2tH/p\n02L0Km8pIc8NBxTCptq7P+ZyABAoxb2hLchp62LdP89fLXHxKNg/EK8VIEFvQfWP\n71RVR3vwlOEkNh6MCApFrhFSaOXOyX7DWxwONQH10BNNC70q1XzJLjz6+qWiIjyY\nli43PHRWp3lAH+TVNvO0Hf7zF84xxm9eY+85AQ8+MNqthe0aQFxMc3IFCHPERnw7\n/WtH+QW9AgG1UoE9Y3vzwqSDxRG2rKR+lK1ToOpd5mkCgYEA6vCe+ixfBedcDfpb\nJWwL9s9djZZ7PjsjZ9vr0Zi3fffMODKsakOM9I2KupaFmR2Vy5o2YT2KaOWNrl/V\nrtM2knEvD32eS1P1hWNtXERSqkLNn/HqPnquOpx4q/RfBuOsd3bHdW9D/PzEmOS/\nnEqpBNY2eveaG9ExM2JRIuSJCs8CgYEA03NI3IJwKQUhWqJLnGJj8VtRNB/N4llZ\nerMsmbVHx5K250+9wCVNppZbGOzgV7PasofqmzxFw/DpaGTM5UlQYXgXZ+bdw2Po\n94xRh2+c86hB1g3PKX7/MQAsHxtWzKrx4eCJ+haNMH3ngdNnjwFBXv/w/g3fTpKh\nSEYjRuPA9iUCgYA5hbyG6EJDwcfKLFgQq4mqXk87kXKPZA9hKHlukgOrNfasc8u/\nINbsMZOb56rCOHoqpZFW8ahn0nyECzbYXSJpbIjnNAyDyQS+Kf/mrVvIEB8kYCjI\nL2VlOFZbU7PIjgn2HgDsAdgVoxXHkYpQi/8iDgi8xs7WrLttJ09SSb48BQKBgCB5\nVxpOig0ytYGm5TfaQIEjxh/cNpFpqX1XoSTmG2UCGkcCDyFAWWGZO9pjHr05aP/8\nMcKvigNir2YR/QnFrauadvXFFg/Y+0Fxcl/7ez5Me/f03PkBgQ20ehI476a1JjrL\nckVPS1XEMFbW8dObnL+LTSanWMFHca+Y3fgYKF1lAoGAN6c1WOQ8rQ79FEHBkLSU\ntW1YIjL0wXEh+oiL2cEIJJR9H8WWLcz8p2debIVcjSJKVYxuqhHBHQE/Qutwf8ZR\nQI3d3NNuFjwmxuNy3uygVhoHFDhbI/lm4QJna+AHDieWnoF1UtacvjLfKvF2b+l2\nTwTbrIZSR68sPu3pTzx3sXs=\n-----END PRIVATE KEY-----' -n harbor"
    command = <<EOC
    cat <<EOF | tee harbor.cnf && \
[ req ]
prompt = no
distinguished_name = dn
req_extensions = req_ext

[ dn ]
CN = harbor.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}
O = Orange
OU = TGI/OLS
L = Val-De-Reuil
ST = France
C = FR

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = harbor.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}
DNS.2 =  harbor-notary.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}
EOF
    apk --no-cache add openssl && \
    openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout harbor.key -out harbor.crt -config harbor.cnf -extensions 'req_ext' && \
    cat harbor.crt && cat harbor.key && \
    kubectl create secret generic harbor-tls -n harbor \
      --from-literal='ca.crt=${var.cluster_ca}' \
      --from-file='tls.crt=./harbor.crt' \
      --from-file='tls.key=./harbor.key'
    EOC
  }

  depends_on = [
      local_file.kubeconfig,
      null_resource.download_kubectl,
      rancher2_namespace.namespace_harbor
  ]
}

# Add Harbor Catalog
resource "rancher2_catalog" "catalog_harbor" {
  count = var.use_harbor == true || var.use_harbor == "true" ? 1 : 0

  name = "harbor"
  project_id = data.rancher2_project.project_system.id
  url = "https://helm.goharbor.io"
  version = "helm_v3"
  refresh = true
  #scope = "global"
  #scope = "cluster"
  scope = "project"

  lifecycle {
    ignore_changes = [project_id, labels]
  }

  depends_on = [null_resource.secret_harbor]
}

# Deploy Harbor app
resource "rancher2_app" "app_harbor" {
  count = var.use_harbor == true || var.use_harbor == "true" ? 1 : 0

  catalog_name = rancher2_catalog.catalog_harbor[count.index].id
  name = "harbor"
  description = "Harbor helm repository"
  project_id = data.rancher2_project.project_system.id
  template_name = "harbor"
  template_version = "1.8.0"
  target_namespace = rancher2_namespace.namespace_harbor[count.index].name
  
  answers = {
    "harborAdminPassword" = "ChangeMeF@st"
    "secretKey" = "MyAwesomeEncrypt"
    "externalURL" = "https://harbor.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}"
    "expose.type" = "ingress"
    "expose.ingress.hosts.core" = "harbor.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}"
    "expose.ingress.hosts.notary" = "harbor-notary.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}"
    "expose.tls.enabled" = "true"
    "expose.tls.secretName" = "harbor-tls"
    "persistence.enabled" = "true"
    "persistence.imageChartStorage.type" = "filesystem"
    "persistence.persistentVolumeClaim.registry.size" = "300Gi"
    "proxy.httpProxy" = var.http_proxy
    "proxy.httpsProxy" = var.https_proxy
    "proxy.noProxy" = var.no_proxy
  }

  lifecycle {
    ignore_changes = [project_id, labels, annotations]
  }

  depends_on = [
    data.rancher2_project.project_system,
    rancher2_namespace.namespace_harbor,
    null_resource.secret_harbor,
    rancher2_catalog.catalog_harbor
  ]
}

###############################################################
###           DEPLOY K8boardS APP
###############################################################

# Create a new rancher2 namespace
resource "rancher2_namespace" "namespace_k8boards" {
  count = var.use_k8boards == true || var.use_k8boards == "true" ? 1 : 0

  name = "k8boards"
  description = "K8boardS namespace"
  project_id = data.rancher2_project.project_system.id

  depends_on = [data.rancher2_project.project_system]
}

# # Create a new rancher2 Project Secret
# resource "rancher2_secret" "dopsi_pullsecret" {
#   count = var.use_k8boards == true || var.use_k8boards == "true" ? 1 : 0

#   name = "registry-fe-dopsi"
#   description = "DOPSI pull secret"
#   project_id = data.rancher2_project.project_system.id
#   namespace_id = rancher2_namespace.namespace_k8boards[count.index].id
#   data = {
#     ".dockerconfigjson" = "eyJhdXRocyI6eyJyZWdpc3RyeS1mZS1kb3BzaS5pdG4uaW50cmFvcmFuZ2UiOnsiYXV0aCI6IllXUnRhVzQ2UkU5UVUwbGtiMk5mTWpBeU1BPT0iLCJwYXNzd29yZCI6IkRPUFNJZG9jXzIwMjAiLCJ1c2VybmFtZSI6ImFkbWluIn19fQo="
#   }

#   lifecycle {
#     ignore_changes = [labels]
#   }

#   depends_on = [rancher2_namespace.namespace_k8boards]
# }

# Create a new Secret
resource "null_resource" "dopsi_pullsecret" {
  count = var.use_k8boards == true || var.use_k8boards == "true" ? 1 : 0
  
  provisioner "local-exec"  {
    command = "kubectl -n kube-system create secret docker-registry registry-fe-dopsi --namespace=k8boards --docker-server=registry-fe-dopsi.itn.intraorange --docker-username=admin --docker-password=DOPSIdoc_2020 --docker-email=edgar.fernandes@orange.com"
  }

  depends_on = [
      local_file.kubeconfig,
      null_resource.download_kubectl,
      rancher2_namespace.namespace_k8boards
  ]
}

# Add a new Rancher2 Cluster Catalog
resource "rancher2_catalog" "catalog_dopsi" {
  count = var.use_k8boards == true || var.use_k8boards == "true" ? 1 : 0

  name = "helm-fe-dopsi"
  project_id = data.rancher2_project.project_system.id
  url = "https://helm-fe-dopsi.itn.intraorange"
  version = "helm_v3"
  refresh = true
  username = "admin"
  password = "DOPSIdoc_2020"
  #scope = "global"
  #scope = "cluster"
  scope = "project"

  lifecycle {
    ignore_changes = [project_id, labels]
  }

  depends_on = [null_resource.dopsi_pullsecret]
}

# Create a new rancher2 App 
resource "rancher2_app" "app_k8boards" {
  count = var.use_k8boards == true || var.use_k8boards == "true" ? 1 : 0

  catalog_name = rancher2_catalog.catalog_dopsi[count.index].id
  name = "k8boards"
  description = "ZDD portal"
  project_id = data.rancher2_project.project_system.id
  template_name = "k8boards"
  template_version = "0.2.2"
  target_namespace = "k8boards"
  wait = true

  answers = {
    "k8boards-frontend.ingress.host" = "k8boards.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}"
    "k8boards-frontend.productionDeployment.env.backendURL.value" = "https://k8boards-apiproxy.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}"
    "k8boards-apiproxy.ingress.host" = "k8boards-apiproxy.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}"
  }

  lifecycle {
    ignore_changes = [project_id, labels, annotations]
  }

  depends_on = [
    rancher2_catalog.catalog_dopsi,
    null_resource.patch_cpo,
    null_resource.add_proxy
  ]
}

# Add cluster to K8boardS
resource "null_resource" "cluster_to_k8boards" {
  count = var.use_k8boards == true || var.use_k8boards == "true" ? 1 : 0

  provisioner "local-exec"  {
    command = <<EOC
    while ! (STATUS_CODE=$(curl -s -o /dev/null -w "%%{http_code}" https://k8boards-apiproxy.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}/status) && [ $${STATUS_CODE} -eq 200 ]);do echo "Waiting K8boardS API being available (sleeping 5s)..." && sleep 5; done && sleep 5 && \
    curl -X POST https://k8boards-apiproxy.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}/auth --data-urlencode username=admin --data-urlencode password=changeme -k -c /tmp/k8boards-session && \
    CLUSTER_ID=$(curl -X POST https://k8boards-apiproxy.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}/setup --data-urlencode name=${var.cluster_name} -k -b /tmp/k8boards-session | jq -r '._id') && \
    curl -X POST https://k8boards-apiproxy.${var.cluster_name}.${var.env_prefix_dns}${var.project_stream}.${var.domain}/setup/kubeconfig/$CLUSTER_ID --form 'file=@/root/.kube/config' -k -b /tmp/k8boards-session && \
    rm /tmp/k8boards-session
    EOC
  }

  depends_on = [
      local_file.kubeconfig,
      null_resource.download_kubectl,
      rancher2_app.app_k8boards
  ]
}

resource "null_resource" "add_proxy_k8boards" {
  count = var.use_k8boards == true || var.use_k8boards == "true" ? (var.http_proxy != "" || var.https_proxy != "" ? 1 : 0) : 0
  
  provisioner "local-exec"  {
    command = <<EOC
    kubectl -n k8boards set env deployment/k8boards-kub HTTP_PROXY=${var.http_proxy} && \
    kubectl -n k8boards set env deployment/k8boards-kub HTTPS_PROXY=${var.https_proxy} && \
    kubectl -n k8boards set env deployment/k8boards-kub NO_PROXY=${var.no_proxy},k8boards-setup
    EOC
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl,
    null_resource.cluster_to_k8boards
  ]
}

###############################################################
###            RESTORE ETCD SNAPSHOT
###############################################################

# resource "null_resource" "etcd_snapshot_restore" {
#   count = var.snapshot_to_restore != "" ? 1 : 0
  
#   triggers = {
#      always_run = timestamp()
#   }

#   connection {
#       type = "ssh"
#       host = element(var.count_etcd == "0" ? openstack_compute_instance_v2.instance_openstack_master.*.network.0.fixed_ip_v4 : openstack_compute_instance_v2.instance_openstack_etcd.*.network.0.fixed_ip_v4, count.index)
#       user = var.ssh_user
#       private_key = var.os_private_key
#   }
  
#   provisioner "file" {
#      content     = rke_cluster.cluster.rke_cluster_yaml
#      destination = "/tmp/cluster.yml"
#   }

#   provisioner "file" {
#      content     = rke_cluster.cluster.rke_state
#      destination = "/tmp/cluster.rkestate"
#   }

#   provisioner "remote-exec"  {
#      inline = [
#     <<EOF
#     export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy} && \
#     cd /tmp && \
#     rke 2>/dev/null 1>/dev/null || \
#     (curl -LO https://github.com/rancher/rke/releases/download/${var.rke_version}/rke_linux-amd64 && mv ./rke_linux-amd64 ./rke && sudo chmod a+x ./rke && sudo mv ./rke /opt/rke/etcd-snapshots) && \
#     (cd /opt/rke/etcd-snapshots && ./rke etcd snapshot-restore --name ${var.snapshot_to_restore} --ignore-docker-version)
#     EOF
#     ]
#   }

#   depends_on = [
#     rancher2_etcd_backup.etcd_backup_before,
#     rancher2_cluster_sync.wait_cluster,
#     rancher2_app.app_k8boards
#   ]
# }

###############################################################
###                   RKE Hardening
###############################################################

# ServiceAccount RKE Hardening
resource "null_resource" "rke_hardening_serviceaccount" {
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = <<EOC
    echo "5.1.5 ServiceAccount Remediation :" && \
    cd ${path.module}/hardening/5.1.5-ServiceAccount/ && chmod +x ./5.1.5-remediation.sh && sh ./5.1.5-remediation.sh
    EOC
  }

  depends_on = [
      local_file.kubeconfig,
      null_resource.add_proxy,
      null_resource.download_kubectl,
      rancher2_app.app_k8boards
  ]
}

# NetworkPolicy RKE Hardening
resource "null_resource" "rke_hardening_networkpolicy" {
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = <<EOC
    echo "5.3.2 NetworkPolicy Remediation :" && \
    cd ${path.module}/hardening/5.3.2-NetworkPolicy/ && chmod +x ./5.3.2-remediation.sh && sh ./5.3.2-remediation.sh
    EOC
  }

  depends_on = [
      local_file.kubeconfig,
      null_resource.download_kubectl,
      null_resource.add_proxy,
      rancher2_app.app_k8boards,
      null_resource.rke_hardening_serviceaccount
  ]
}

###############################################################
###            CREATE onetime ETCD SNAPSHOT
###############################################################

# Create a new rancher2 Etcd Backup
resource "rancher2_etcd_backup" "etcd_backup_after" {
  cluster_id = rancher2_cluster.cluster.id
  #name = "k8s-cluster-${var.cluster_name}-etcd-backup-onetime-after"
  name = "etcd-after"
  #filename = "${timestamp()}_etcd-after"

  backup_config {
    enabled                 = var.enable_etcd_snapshots
    interval_hours          = var.k8s_backup_interval
    retention               = var.k8s_backup_retention
    s3_backup_config {
      access_key            = var.s3_access_key
      secret_key            = var.s3_secret_key
      bucket_name           = var.s3_bucket_name
      folder                = "k8s-cluster-${var.cluster_name}-etcd-backup"
      region                = var.s3_region
      endpoint              = var.s3_service_endpoint
    }
  }

  depends_on = [
    rancher2_etcd_backup.etcd_backup_before,
    rancher2_cluster_sync.wait_cluster,
    rancher2_app.app_k8boards
  ]
  # depends_on = [null_resource.etcd_snapshot_restore]
}

###############################################################
###               STORAGE CLASS DEPLOYMENT
###############################################################

# # Create a new rancher2 namespace
# resource "rancher2_namespace" "namespace_storage" {
#   name = "longhorn-system"
#   description = "Storage namespace"
#   project_id = data.rancher2_project.project_system.id

#   depends_on = [data.rancher2_project.project_system]
# }

# # Create a new rancher2 App 
# resource "rancher2_app" "app_storage" {
#   catalog_name = "library"
#   name = "longhorn-system"
#   description = "Longhorn app"
#   project_id = data.rancher2_project.project_system.id
#   template_name = "longhorn"
#   template_version = "1.0.0"
#   target_namespace = rancher2_namespace.namespace_storage.name
#   answers = {
#     "image.defaultImage" = true
#     "ingress.enabled" = false
#     "longhorn.default_setting" = false
#     "privateRegistry.registryPasswd" = ""
#     "privateRegistry.registryUrl" = ""
#     "privateRegistry.registryUser" = ""
#     "service.ui.type" = "Rancher-Proxy"
#   }
#   
#   lifecycle {
#     ignore_changes = [labels, annotations]
#   }
#   timeouts {
#     create = "5m"
#   }

#    depends_on = [rancher2_namespace.namespace_storage]
# }

###############################################################
###               WAIT STORAGE CLASS
###############################################################

# resource "null_resource" "wait_storage" {
#   provisioner "local-exec"  {
#     command = "sleep 300"
#   }

#   depends_on = [rancher2_namespace.namespace_storage]
# }

###############################################################

