terraform {
  backend "s3" {
    bucket = "S3_BUCKET_NAME"
    key    = "S3_BUCKET_KEY"

    # Deactivate the AWS specific behaviours
    #
    # https://www.terraform.io/docs/backends/types/s3.html#skip_credentials_validation
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
  }
}
