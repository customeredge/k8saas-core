###############################################################
###                   Common Providers
###############################################################

provider "null" {
  #version = "~> 2.1"
  version = "~> 3.1.0"
}

provider "local" {
  version = "~> 2.0"
}

###############################################################
###                      Script Version
###############################################################

resource "null_resource" "display_script_version" {
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
      command = <<EOC
      echo ""
      echo ""
      echo "############################################################################################"
      echo "################################### SCRIPT VERSION: ${local.script_version} ##################################"
      echo "############################################################################################"
      echo ""
      echo ""
      EOC
  }
}

###############################################################
###                      Cluster informations
###############################################################

resource "null_resource" "display_cluster_info" {
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = <<EOC
      echo ""
      echo ""
      echo "############################################################################################"
      echo "######################################## CLUSTER INFO ######################################"
      echo "############################################################################################"
      echo ""
      echo "########### --------------------------------------------------------------------------------"
      echo "########### CLUSTER_NAME : ${local.json_data.cluster_name}"
      echo "########### K8S_VERSION : ${local.json_data.parameters.cluster.client.k8s_version}"
      echo "########### CLUSTER_FQDN : ${local.json_data.parameters.cluster.client.cluster_fqdn}"
      echo "########### ENV_PREFIX : ${local.env_prefix}"
      echo "########### CLOUD : ${local.cloud}"
      echo "########### --------------------------------------------------------------------------------"
      echo "########### RANCHER_URL : ${local.rancher_url}"
      echo "########### --------------------------------------------------------------------------------"
      echo "########### IMAGE_NAME : ${local.json_data.parameters.cluster.admin.image_name}"
      echo "########### --------------------------------------------------------------------------------"
      echo "########### COUNT_WORKER : ${local.json_data.parameters.cluster.client.count_worker}"
      echo "########### FLAVOR_NAME_WORKER : ${local.json_data.parameters.cluster.client.flavor_name_worker}"
      echo "########### --------------------------------------------------------------------------------"
      echo "########### COUNT_WORKER_GPU : ${local.json_data.parameters.cluster.client.count_worker_gpu}"
      echo "########### FLAVOR_NAME_WORKER_GPU : ${local.json_data.parameters.cluster.client.flavor_name_worker_gpu}"
      echo "########### --------------------------------------------------------------------------------"
      echo "########### COUNT_MASTER_ETCD : ${local.json_data.parameters.cluster.admin.count_master_etcd}"
      echo "########### FLAVOR_NAME_MASTER_ETCD : ${local.json_data.parameters.cluster.admin.flavor_name_master_etcd}"
      echo "########### --------------------------------------------------------------------------------"
      echo "########### COUNT_MASTER : ${local.json_data.parameters.cluster.admin.count_master}"
      echo "########### FLAVOR_NAME_MASTER : ${local.json_data.parameters.cluster.admin.flavor_name_master}"
      echo "########### --------------------------------------------------------------------------------"
      echo "########### COUNT_ETCD : ${local.json_data.parameters.cluster.admin.count_etcd}"
      echo "########### FLAVOR_NAME_ETCD : ${local.json_data.parameters.cluster.admin.flavor_name_etcd}"
      echo "########### --------------------------------------------------------------------------------"
      echo ""
      echo "############################################################################################"
      EOC
  }

  depends_on = [null_resource.display_script_version]
}

###############################################################
###                      Modules
###############################################################

# Execute k8s-openstack-rke module
module "k8s_openstack_rke" {
  source = "./modules/k8s-openstack-rke"

  # KUBERNETES
  cluster_name              = local.json_data.cluster_name
  k8s_version               = local.json_data.parameters.cluster.client.k8s_version
  cluster_fqdn              = local.json_data.parameters.cluster.client.cluster_fqdn
  enable_etcd_snapshots     = local.json_data.parameters.cluster.admin.enable_etcd_snapshots
  k8s_backup_interval       = local.json_data.parameters.cluster.admin.k8s_backup_interval
  k8s_backup_retention      = local.json_data.parameters.cluster.admin.k8s_backup_retention
  cluster_ca                = local.cluster_ca
  
  # METADATA
  env_prefix                = local.env_prefix
  env_prefix_dns            = local.env_prefix_dns
  project_stream            = local.json_data.parameters.cluster.admin.project_stream
  domain                    = local.json_data.parameters.cluster.admin.domain
  cloud                     = local.cloud

  # SCALE
  count_master_etcd         = local.json_data.parameters.cluster.admin.count_master_etcd
  count_master              = local.json_data.parameters.cluster.admin.count_master
  count_etcd                = local.json_data.parameters.cluster.admin.count_etcd
  count_worker              = local.json_data.parameters.cluster.client.count_worker
  count_worker_gpu          = local.json_data.parameters.cluster.client.count_worker_gpu

  # IMAGE
  image_name                = local.json_data.parameters.cluster.admin.image_name

  # FLAVORS
  flavor_name_master_etcd   = local.json_data.parameters.cluster.admin.flavor_name_master_etcd
  flavor_name_master        = local.json_data.parameters.cluster.admin.flavor_name_master
  flavor_name_etcd          = local.json_data.parameters.cluster.admin.flavor_name_etcd
  flavor_name_worker        = local.json_data.parameters.cluster.client.flavor_name_worker
  flavor_name_worker_gpu    = local.json_data.parameters.cluster.client.flavor_name_worker_gpu

  # TFSTATE COMMON
  use_remote_state          = local.json_data.parameters.cluster.admin.use_remote_state
  backend_tf                = local.json_data.parameters.cluster.admin.backend_tf
  backend_path              = var.backend_path

  # CONSUL
  use_consul                = local.json_data.parameters.cluster.admin.use_consul
  CONSUL_HTTP_ADDR          = local.consul_address
  CONSUL_HTTP_TOKEN         = local.consul_token

  # S3
  s3_service_endpoint       = local.s3_service_endpoint
  s3_bucket_name            = local.s3_bucket_name
  s3_region                 = local.s3_region
  s3_access_key             = local.s3_access_key
  s3_secret_key             = local.s3_secret_key

  # STORAGE
  additional_storage        = local.additional_storage
  additional_storage_size   = local.additional_storage_size
  enable_etcd_ssd           = local.enable_etcd_ssd

  # LOGGING
  enable_logging            = local.json_data.parameters.cluster.admin.enable_logging
  suricate_endpoint         = local.suricate_endpoint
  suricate_appid            = local.suricate_appid
  suricate_ca               = local.suricate_ca
  suricate_cert             = local.suricate_cert
  suricate_key              = local.suricate_key

  # MONITORING & METRICS
  enable_cluster_monitoring = local.json_data.parameters.cluster.admin.enable_cluster_monitoring
  vmc_endpoint_target1      = local.vmc_endpoint_target1
  #vmc_endpoint_target2      = local.vmc_endpoint_target2
  vmc_username              = local.vmc_username
  vmc_password              = local.vmc_password

  # NETWORK
  az_list                   = split(",", local.az_list)
  internal_network_name     = local.internal_network_name
  external_network_name     = local.external_network_name
  internal_subnet_id        = local.internal_subnet_id
  external_subnet_id        = local.external_subnet_id
  create_loadbalancer       = local.json_data.parameters.cluster.admin.create_loadbalancer
  loadbalancer_api_eip      = local.loadbalancer_api_eip
  loadbalancer_worker_eip   = local.loadbalancer_worker_eip
  sg_default                = local.sg_default
  sg_api                    = local.sg_api
  sg_etcd                   = local.sg_etcd
  sg_worker                 = local.sg_worker
  cni                       = local.json_data.parameters.cluster.admin.cni
  cluster_domain            = local.json_data.parameters.cluster.admin.cluster_domain
  cluster_cidr              = local.json_data.parameters.cluster.admin.cluster_cidr
  service_cidr              = local.json_data.parameters.cluster.admin.service_cidr
  cluster_dns_server        = local.json_data.parameters.cluster.admin.cluster_dns_server

  # SSH
  ssh_user                  = local.json_data.parameters.cluster.admin.ssh_user
  key_pair                  = local.key_pair

  # PROXY
  http_proxy                = local.json_data.parameters.cluster.admin.http_proxy
  https_proxy               = local.json_data.parameters.cluster.admin.https_proxy
  no_proxy                  = local.json_data.parameters.cluster.admin.no_proxy

  # ETCD
  etcd_volume_size_gb       = local.json_data.parameters.cluster.admin.etcd_volume_size_gb

  # DOCKER
  docker_version            = local.json_data.parameters.cluster.client.docker_version
  docker_volume_size_gb     = local.json_data.parameters.cluster.admin.docker_volume_size_gb
  registry_mirror           = local.json_data.parameters.cluster.admin.registry_mirror
  docker_username           = local.docker_username
  docker_password           = local.docker_password

  # OPENSTACK
  os_auth_url               = local.os_auth_url
  os_domain_name            = local.os_domain_name
  os_tenant_id              = local.os_tenant_id
  os_username               = local.os_username
  os_password               = local.os_password
  os_region_name            = local.os_region_name
  os_private_key            = local.os_private_key
  use_openstack_cinder_csi  = local.use_openstack_cinder_csi

  # RANCHER
  rancher_url               = local.rancher_url
  rancher_access_key        = local.rancher_access_key
  rancher_secret_key        = local.rancher_secret_key

  # K8BOARDS
  use_harbor                = local.json_data.parameters.cluster.client.use_harbor

  # K8BOARDS
  use_k8boards              = local.json_data.parameters.cluster.client.use_k8boards

  # SNAPSHOT RESTORE
  snapshot_to_restore       = local.snapshot_to_restore
}
