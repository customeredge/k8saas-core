###############################################################
###                   Common Providers
###############################################################

terraform {
  required_providers {
    rancher2 = {
      source = "rancher/rancher2"
      version = "~> 1.20.0"
    }
  }
}

provider "null" {
  version = "~> 2.1"
}

provider "local" {
  version = "~> 2.0"
}

provider "rancher2" {
  insecure   = "true"

  api_url     = local.rancher_url
  access_key  = local.rancher_access_key
  secret_key  = local.rancher_secret_key
}

provider "google" {
  version = "~> 3.42.0"
  credentials = local.cluster_gcp_credentials
}

###############################################################
###                      Script Version
###############################################################

resource "null_resource" "display_script_version" {
  triggers = {
    always_run = timestamp()
  }
    
  provisioner "local-exec"  {
      command = <<EOC
      echo ""
      echo ""
      echo "############################################################################################"
      echo "################################### SCRIPT VERSION: ${local.script_version} ##################################"
      echo "############################################################################################"
      echo ""
      echo ""
      EOC
  }
}

###############################################################
###                      Resources
###############################################################

# Create GCP credentials into Rancher
resource "rancher2_cloud_credential" "credentials-gcp" {
  name = "credentials-gcp"
  description= "GCP credentials"
  google_credential_config {
    auth_encoded_json = local.cluster_gcp_credentials
  }
}


###############################################################

/* 
#
## To be added into K8NaaS
#

# Create network
resource "google_compute_network" "network-gke" {
  name                    = "gke-network"
  auto_create_subnetworks = false
}

*/

data "google_compute_network" "network-gke" {
  #name                    = "gke-network"
  name                    = local.sub_network
}

# Create sub-network
resource "google_compute_subnetwork" "subnetwork-gke" {
  name                       = "${local.region}-${local.json_data.cluster_name}"
  #name                       = "gke-network"
  #ip_cidr_range              = "172.16.0.0/20"
  ip_cidr_range              = "10.2.0.0/24"
  region                     = local.region
  network                    = data.google_compute_network.network-gke.id
  secondary_ip_range = [
    {
      range_name    = "${local.region}-${local.json_data.cluster_name}-pods"
      ip_cidr_range = local.cluster_cidr
    },
    {
      range_name    = "${local.region}-${local.json_data.cluster_name}-services"
      ip_cidr_range = local.service_cidr
    }
  ]
}

###############################################################
###                      Modules
###############################################################

module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  project_id                 = local.project_id
  name                       = local.json_data.cluster_name
  region                     = local.region
  zones                      = split(",", local.az_list)
  network                    = local.network_name
  #subnetwork                 = local.sub_network
  subnetwork                 = "${local.region}-${local.json_data.cluster_name}"
  ip_range_pods              = "${local.region}-${local.json_data.cluster_name}-pods"
  ip_range_services          = "${local.region}-${local.json_data.cluster_name}-services"
  http_load_balancing        = true
  horizontal_pod_autoscaling = true
  network_policy             = false
  create_service_account     = false
  enable_private_endpoint    = false
  enable_private_nodes       = true
  kubernetes_version         = local.json_data.parameters.cluster.client.k8s_version

  node_pools = [
    {
      name                      = "default-node-pool"
      machine_type              = local.json_data.parameters.cluster.client.flavor_name_worker
      node_locations            = local.az_list
      min_count                 = 1
      max_count                 = 10
      local_ssd_count           = 0
      disk_size_gb              = 100
      disk_type                 = local.disk_type
      image_type                = local.image_type
      auto_repair               = false
      auto_upgrade              = false
      service_account           = local.client_email
      preemptible               = false
      #initial_node_count        = local.json_data.parameters.cluster.client.count_worker
      initial_node_count        = 1
      enable_secure_boot        = true
    },
  ]

  
  
  master_authorized_networks = [
    {
      cidr_block   = "10.0.0.0/8"
      display_name = "VPC"
    },
    {
      cidr_block   = "80.12.0.0/16"
      display_name = "Orange External Network"
    },
    {
      cidr_block   = "90.84.0.0/16"
      display_name = "FE External Network"
    }
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {}

    default-node-pool = {
      default-node-pool = true
    }
  }

  /* node_pools_metadata = {
    all = {}

    default-node-pool = {
      node-pool-metadata-custom-value = "my-node-pool"
    }
  }

  node_pools_taints = {
    all = []

    default-node-pool = [
      {
        key    = "default-node-pool"
        value  = true
        effect = "PREFER_NO_SCHEDULE"
      },
    ]
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-node-pool",
    ]
  } */
}

#########################################################################

# Import GKE cluster into Rancher
resource "rancher2_cluster" "import-gke-cluster" {
  name = local.json_data.cluster_name
  description = "GKE imported cluster"
  gke_config_v2 {
    name = local.json_data.cluster_name
    google_credential_secret = rancher2_cloud_credential.credentials-gcp.id
    region = local.region # Zone argument could also be used instead of region
    project_id = local.project_id
    imported = true
  }

  timeouts {
    create = "15m"
  }

  #depends_on = [rancher2_cloud_credential.credentials-gcp]
  depends_on = [module.gke]
}

#########################################################################
