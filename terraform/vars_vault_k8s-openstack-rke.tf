###############################################################
###             Get Openstack/FE Vault Secrets
###############################################################

data "vault_generic_secret" "openstack" {
  path = "${var.vault_kv_path}/openstack/${local.json_data.parameters.cluster.client.pf_zone}"
}

locals {
  os_auth_url               = data.vault_generic_secret.openstack.data["OS_AUTH_URL"]
  os_domain_name            = data.vault_generic_secret.openstack.data["OS_DOMAIN_NAME"]
  os_tenant_id              = data.vault_generic_secret.openstack.data["OS_TENANT_ID"]
  os_username               = data.vault_generic_secret.openstack.data["OS_USERNAME"]
  os_password               = data.vault_generic_secret.openstack.data["OS_PASSWORD"]
  os_region_name            = data.vault_generic_secret.openstack.data["OS_REGION_NAME"]
  os_private_key            = data.vault_generic_secret.openstack.data["OS_PRIVATE_KEY"]
  cluster_ca                = data.vault_generic_secret.openstack.data["CLUSTER_CA"]
  az_list                   = data.vault_generic_secret.openstack.data["az_list"]
  sg_default                = data.vault_generic_secret.openstack.data["sg_default"]
  sg_api                    = data.vault_generic_secret.openstack.data["sg_api"]
  sg_etcd                   = data.vault_generic_secret.openstack.data["sg_etcd"]
  sg_worker                 = data.vault_generic_secret.openstack.data["sg_worker"]
  cloud                     = data.vault_generic_secret.openstack.data["cloud"]
  key_pair                  = data.vault_generic_secret.openstack.data["key_pair"]
  internal_network_name     = data.vault_generic_secret.openstack.data["internal_network_name"]
  external_network_name     = data.vault_generic_secret.openstack.data["external_network_name"]
  internal_subnet_id        = data.vault_generic_secret.openstack.data["internal_subnet_id"]
  external_subnet_id        = data.vault_generic_secret.openstack.data["external_subnet_id"]
  loadbalancer_api_eip      = data.vault_generic_secret.openstack.data["loadbalancer_api_eip"]
  loadbalancer_worker_eip   = data.vault_generic_secret.openstack.data["loadbalancer_worker_eip"]
  use_openstack_cinder_csi  = data.vault_generic_secret.openstack.data["use_openstack_cinder_csi"]
}
