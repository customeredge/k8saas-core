###############################################################
###                   Get json parameters
###############################################################

locals {
  script_version            = "0.1.1"

  json_data = jsondecode(file(var.parameters))
  
  env_prefix                = local.json_data.parameters.cluster.client.env_prefix
  env_prefix_dns            = local.json_data.parameters.cluster.admin.use_env_prefix_dns == "true" || local.json_data.parameters.cluster.admin.use_env_prefix_dns == true ? (local.env_prefix == "pr" ? "" : "${local.env_prefix}-") : ""
 
  additional_storage        = lookup(local.json_data.parameters.cluster.client,"additional_storage","null") != "null" ? local.json_data.parameters.cluster.client.additional_storage : ""
  additional_storage_size   = element(split(",", local.additional_storage),2)

  snapshot_to_restore       = lookup(local.json_data.parameters.cluster.client,"snapshot_to_restore","null") != "null" ? local.json_data.parameters.cluster.client.snapshot_to_restore : ""

  enable_etcd_ssd           = lookup(local.json_data.parameters.cluster.admin,"enable_etcd_ssd","null") != "null" ? local.json_data.parameters.cluster.admin.enable_etcd_ssd : "false"

  cluster_cidr              = lookup(local.json_data.parameters.cluster.admin,"cluster_cidr","null") != "null" ? local.json_data.parameters.cluster.admin.cluster_cidr : "false"
  service_cidr              = lookup(local.json_data.parameters.cluster.admin,"service_cidr","null") != "null" ? local.json_data.parameters.cluster.admin.service_cidr : "false"
  
  rancher_url               = local.json_data.parameters.infrastructure.rancher.RANCHER_URL
  rancher_access_key        = local.json_data.parameters.infrastructure.rancher.RANCHER_ACCESS_KEY
  rancher_secret_key        = local.json_data.parameters.infrastructure.rancher.RANCHER_SECRET_KEY

  docker_username           = lookup(local.json_data.parameters.infrastructure,"docker","null") != "null" ? local.json_data.parameters.infrastructure.docker.DOCKER_USERNAME : ""
  docker_password           = lookup(local.json_data.parameters.infrastructure,"docker","null") != "null" ? local.json_data.parameters.infrastructure.docker.DOCKER_PASSWORD : ""

  consul_address            = lookup(local.json_data.parameters.infrastructure,"consul","null") != "null" ? local.json_data.parameters.infrastructure.consul.CONSUL_HTTP_ADDR : ""
  consul_token              = lookup(local.json_data.parameters.infrastructure,"consul","null") != "null" ? local.json_data.infrastructure.consul.CONSUL_HTTP_TOKEN : ""

  s3_service_endpoint       = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_SERVICE_ENDPOINT : ""
  s3_bucket_name            = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_BUCKET_NAME : ""
  s3_region                 = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_REGION : ""
  s3_access_key             = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_ACCESS_KEY : ""
  s3_secret_key             = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_SECRET_KEY : ""

  suricate_endpoint         = lookup(local.json_data.parameters.infrastructure,"suricate","null") != "null" ? local.json_data.parameters.infrastructure.suricate.SURICATE_ENDPOINT : ""
  suricate_appid            = lookup(local.json_data.parameters.infrastructure,"suricate","null") != "null" ? local.json_data.parameters.infrastructure.suricate.SURICATE_APPID : ""
  suricate_ca               = lookup(local.json_data.parameters.infrastructure,"suricate","null") != "null" ? local.json_data.parameters.infrastructure.suricate.SURICATE_CA : ""
  suricate_cert             = lookup(local.json_data.parameters.infrastructure,"suricate","null") != "null" ? local.json_data.parameters.infrastructure.suricate.SURICATE_CERT : ""
  suricate_key              = lookup(local.json_data.parameters.infrastructure,"suricate","null") != "null" ? local.json_data.parameters.infrastructure.suricate.SURICATE_KEY : ""

  vmc_endpoint_target1      = lookup(local.json_data.parameters.infrastructure,"vmc","null") != "null" ? local.json_data.parameters.infrastructure.vmc.VMC_ENDPOINT_TARGET1 : ""
  #vmc_endpoint_target2      = lookup(local.json_data.parameters.infrastructure,"vmc","null") != "null" ? local.json_data.parameters.infrastructure.vmc.VMC_ENDPOINT_TARGET2 : ""
  vmc_username              = lookup(local.json_data.parameters.infrastructure,"vmc","null") != "null" ? local.json_data.parameters.infrastructure.vmc.VMC_USERNAME : ""
  vmc_password              = lookup(local.json_data.parameters.infrastructure,"vmc","null") != "null" ? local.json_data.parameters.infrastructure.vmc.VMC_PASSWORD : ""
}
